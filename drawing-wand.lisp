(in-package :cl-magick)

;Copyright (c) 2005 Dave Watson

;Permission is hereby granted, free of charge, to any person obtaining
;a copy of this software and associated documentation files (the
;"Software"), to deal in the Software without restriction, including
;without limitation the rights to use, copy, modify, merge, publish,
;distribute, sublicense, and/or sell copies of the Software, and to
;permit persons to whom the Software is furnished to do so, subject to
;the following conditions:

;The above copyright notice and this permission notice shall be
;included in all copies or substantial portions of the Software.

;THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
;LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(uffi:def-function ("DrawGetTextAlignment" DrawGetTextAlignment)
                   ( (dwand (* DrawingWand)))
                   :returning AlignType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetClipPath" DrawGetClipPath)
                   ( (dwand (* DrawingWand)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetException" DrawGetException)
                   ( (dwand (* DrawingWand)) (exceptiontype (* ExceptionType)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetFont" DrawGetFont)
                   ( (dwand (* DrawingWand)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetFontFamily" DrawGetFontFamily)
                   ( (dwand (* DrawingWand)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetTextEncoding" DrawGetTextEncoding)
                   ( (dwand (* DrawingWand)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetVectorGraphics" DrawGetVectorGraphics)
                   ( (dwand (* DrawingWand)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetClipUnits" DrawGetClipUnits)
                   ( (dwand (* DrawingWand)))
                   :returning ClipPathUnits
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetTextDecoration" DrawGetTextDecoration)
                   ( (dwand (* DrawingWand)))
                   :returning DecorationType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetFillAlpha" DrawGetFillAlpha)
                   ( (dwand (* DrawingWand)))
                   :returning :double
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetFontSize" DrawGetFontSize)
                   ( (dwand (* DrawingWand)))
                   :returning :double
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetStrokeDashArray" DrawGetStrokeDashArray)
                   ( (dwand (* DrawingWand)) (lint (* :unsigned-long)))
                   :returning (* :double)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetStrokeDashOffset" DrawGetStrokeDashOffset)
                   ( (dwand (* DrawingWand)))
                   :returning :double
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetStrokeAlpha" DrawGetStrokeAlpha)
                   ( (dwand (* DrawingWand)))
                   :returning :double
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetStrokeWidth" DrawGetStrokeWidth)
                   ( (dwand (* DrawingWand)))
                   :returning :double
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PeekDrawingWand" PeekDrawingWand)
                   ( (dwand (* DrawingWand)))
                   :returning (* DrawInfo)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("CloneDrawingWand" CloneDrawingWand)
                   ( (dwand (* DrawingWand)))
                   :returning (* DrawingWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DestroyDrawingWand" DestroyDrawingWand)
                   ( (dwand (* DrawingWand)))
                   :returning (* DrawingWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawAllocateWand" DrawAllocateWand)
                   ( (drawinfo (* DrawInfo)) (image (* Image)))
                   :returning (* DrawingWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("NewDrawingWand" NewDrawingWand)
                   ()
                   :returning (* DrawingWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetClipRule" DrawGetClipRule)
                   ( (dwand (* DrawingWand)))
                   :returning FillRule
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetFillRule" DrawGetFillRule)
                   ( (dwand (* DrawingWand)))
                   :returning FillRule
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetGravity" DrawGetGravity)
                   ( (dwand (* DrawingWand)))
                   :returning GravityType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetStrokeLineCap" DrawGetStrokeLineCap)
                   ( (dwand (* DrawingWand)))
                   :returning LineCap
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetStrokeLineJoin" DrawGetStrokeLineJoin)
                   ( (dwand (* DrawingWand)))
                   :returning LineJoin
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawClearException" DrawClearException)
                   ( (dwand (* DrawingWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawComposite" DrawComposite)
                   ( (dwand (* DrawingWand)) (compositeoperator CompositeOperator) (dint :double) (dint2 :double) (dint3 :double) (dint4 :double) (wand (* MagickWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetStrokeAntialias" DrawGetStrokeAntialias)
                   ( (dwand (* DrawingWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetTextAntialias" DrawGetTextAntialias)
                   ( (dwand (* DrawingWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPopPattern" DrawPopPattern)
                   ( (dwand (* DrawingWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPushPattern" DrawPushPattern)
                   ( (dwand (* DrawingWand)) (str :cstring) (dint :double) (dint2 :double) (dint3 :double) (dint4 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawRender" DrawRender)
                   ( (dwand (* DrawingWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetClipPath" DrawSetClipPath)
                   ( (dwand (* DrawingWand)) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetFillPatternURL" DrawSetFillPatternURL)
                   ( (dwand (* DrawingWand)) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetFont" DrawSetFont)
                   ( (dwand (* DrawingWand)) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetFontFamily" DrawSetFontFamily)
                   ( (dwand (* DrawingWand)) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetStrokeDashArray" DrawSetStrokeDashArray)
                   ( (dwand (* DrawingWand)) (lint :unsigned-long) (dint (* :double)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetStrokePatternURL" DrawSetStrokePatternURL)
                   ( (dwand (* DrawingWand)) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetVectorGraphics" DrawSetVectorGraphics)
                   ( (dwand (* DrawingWand)) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("IsDrawingWand" IsDrawingWand)
                   ( (dwand (* DrawingWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PopDrawingWand" PopDrawingWand)
                   ( (dwand (* DrawingWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PushDrawingWand" PushDrawingWand)
                   ( (dwand (* DrawingWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetFontStretch" DrawGetFontStretch)
                   ( (dwand (* DrawingWand)))
                   :returning StretchType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetFontStyle" DrawGetFontStyle)
                   ( (dwand (* DrawingWand)))
                   :returning StyleType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetFontWeight" DrawGetFontWeight)
                   ( (dwand (* DrawingWand)))
                   :returning :unsigned-long
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetStrokeMiterLimit" DrawGetStrokeMiterLimit)
                   ( (dwand (* DrawingWand)))
                   :returning :unsigned-long
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("ClearDrawingWand" ClearDrawingWand)
                   ( (dwand (* DrawingWand)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawAffine" DrawAffine)
                   ( (dwand (* DrawingWand)) (affinematrix (* AffineMatrix)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawAnnotation" DrawAnnotation)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double) (str (* :unsigned-char)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawArc" DrawArc)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double) (dint3 :double) (dint4 :double) (dint5 :double) (dint6 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawBezier" DrawBezier)
                   ( (dwand (* DrawingWand)) (lint :unsigned-long) (pinfo (* PointInfo)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawCircle" DrawCircle)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double) (dint3 :double) (dint4 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawColor" DrawColor)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double) (paintmethod PaintMethod))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawComment" DrawComment)
                   ( (dwand (* DrawingWand)) (* :char))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawEllipse" DrawEllipse)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double) (dint3 :double) (dint4 :double) (dint5 :double) (dint6 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetFillColor" DrawGetFillColor)
                   ( (dwand (* DrawingWand)) (wand (* PixelWand)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetStrokeColor" DrawGetStrokeColor)
                   ( (dwand (* DrawingWand)) (wand (* PixelWand)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetTextUnderColor" DrawGetTextUnderColor)
                   ( (dwand (* DrawingWand)) (wand (* PixelWand)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawLine" DrawLine)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double) (dint3 :double) (dint4 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawMatte" DrawMatte)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double) (paintmethod PaintMethod))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathClose" DrawPathClose)
                   ( (dwand (* DrawingWand)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathCurveToAbsolute" DrawPathCurveToAbsolute)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double) (dint3 :double) (dint4 :double) (dint5 :double) (dint6 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathCurveToRelative" DrawPathCurveToRelative)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double) (dint3 :double) (dint4 :double) (dint5 :double) (dint6 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathCurveToQuadraticBezierAbsolute" DrawPathCurveToQuadraticBezierAbsolute)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double) (dint3 :double) (dint4 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathCurveToQuadraticBezierRelative" DrawPathCurveToQuadraticBezierRelative)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double) (dint3 :double) (dint4 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathCurveToQuadraticBezierSmoothAbsolute" DrawPathCurveToQuadraticBezierSmoothAbsolute)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathCurveToQuadraticBezierSmoothRelative" DrawPathCurveToQuadraticBezierSmoothRelative)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathCurveToSmoothAbsolute" DrawPathCurveToSmoothAbsolute)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double) (dint3 :double) (dint4 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathCurveToSmoothRelative" DrawPathCurveToSmoothRelative)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double) (dint3 :double) (dint4 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathEllipticArcAbsolute" DrawPathEllipticArcAbsolute)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double) (dint3 :double) (bool MagickBooleanType) (bool2 MagickBooleanType) (dint4 :double) (dint5 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathEllipticArcRelative" DrawPathEllipticArcRelative)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double) (dint3 :double) (bool MagickBooleanType) (bool2 MagickBooleanType) (dint4 :double) (dint5 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathFinish" DrawPathFinish)
                   ( (dwand (* DrawingWand)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathLineToAbsolute" DrawPathLineToAbsolute)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathLineToRelative" DrawPathLineToRelative)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathLineToHorizontalAbsolute" DrawPathLineToHorizontalAbsolute)
                   ( (dwand (* DrawingWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathLineToHorizontalRelative" DrawPathLineToHorizontalRelative)
                   ( (dwand (* DrawingWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathLineToVerticalAbsolute" DrawPathLineToVerticalAbsolute)
                   ( (dwand (* DrawingWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathLineToVerticalRelative" DrawPathLineToVerticalRelative)
                   ( (dwand (* DrawingWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathMoveToAbsolute" DrawPathMoveToAbsolute)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathMoveToRelative" DrawPathMoveToRelative)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPathStart" DrawPathStart)
                   ( (dwand (* DrawingWand)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPoint" DrawPoint)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPolygon" DrawPolygon)
                   ( (dwand (* DrawingWand)) (lint :unsigned-long) (pinfo (* PointInfo)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPolyline" DrawPolyline)
                   ( (dwand (* DrawingWand)) (lint :unsigned-long) (pinfo (* PointInfo)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPopClipPath" DrawPopClipPath)
                   ( (dwand (* DrawingWand)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPopDefs" DrawPopDefs)
                   ( (dwand (* DrawingWand)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPushClipPath" DrawPushClipPath)
                   ( (dwand (* DrawingWand)) (str :cstring))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPushDefs" DrawPushDefs)
                   ( (dwand (* DrawingWand)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawRectangle" DrawRectangle)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double) (dint3 :double) (dint4 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawRotate" DrawRotate)
                   ( (dwand (* DrawingWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawRoundRectangle" DrawRoundRectangle)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double) (dint3 :double) (dint4 :double) (dint5 :double) (dint6 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawScale" DrawScale)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetClipRule" DrawSetClipRule)
                   ( (dwand (* DrawingWand)) (fillrule FillRule))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetClipUnits" DrawSetClipUnits)
                   ( (dwand (* DrawingWand)) (pathunits ClipPathUnits))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetFillColor" DrawSetFillColor)
                   ( (dwand (* DrawingWand)) (wand (* PixelWand)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetFillAlpha" DrawSetFillAlpha)
                   ( (dwand (* DrawingWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetFillRule" DrawSetFillRule)
                   ( (dwand (* DrawingWand)) (fillrule FillRule))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetFontSize" DrawSetFontSize)
                   ( (dwand (* DrawingWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetFontStretch" DrawSetFontStretch)
                   ( (dwand (* DrawingWand)) (stretchtype StretchType))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetFontStyle" DrawSetFontStyle)
                   ( (dwand (* DrawingWand)) (styletype StyleType))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetFontWeight" DrawSetFontWeight)
                   ( (dwand (* DrawingWand)) (lint :unsigned-long))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetGravity" DrawSetGravity)
                   ( (dwand (* DrawingWand)) (gravitytype GravityType))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSkewX" DrawSkewX)
                   ( (dwand (* DrawingWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSkewY" DrawSkewY)
                   ( (dwand (* DrawingWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetStrokeAntialias" DrawSetStrokeAntialias)
                   ( (dwand (* DrawingWand)) (bool MagickBooleanType))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetStrokeColor" DrawSetStrokeColor)
                   ( (dwand (* DrawingWand)) (wand (* PixelWand)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetStrokeDashOffset" DrawSetStrokeDashOffset)
                   ( (dwand (* DrawingWand)) (dashoffset :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetStrokeLineCap" DrawSetStrokeLineCap)
                   ( (dwand (* DrawingWand)) (linecap LineCap))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetStrokeLineJoin" DrawSetStrokeLineJoin)
                   ( (dwand (* DrawingWand)) (linejoin LineJoin))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetStrokeMiterLimit" DrawSetStrokeMiterLimit)
                   ( (dwand (* DrawingWand)) (lint :unsigned-long))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetStrokeAlpha" DrawSetStrokeAlpha)
                   ( (dwand (* DrawingWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetStrokeWidth" DrawSetStrokeWidth)
                   ( (dwand (* DrawingWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetTextAlignment" DrawSetTextAlignment)
                   ( (dwand (* DrawingWand)) (aligntype AlignType))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetTextAntialias" DrawSetTextAntialias)
                   ( (dwand (* DrawingWand)) (bool MagickBooleanType))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetTextDecoration" DrawSetTextDecoration)
                   ( (dwand (* DrawingWand)) (decorationtype DecorationType))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetTextEncoding" DrawSetTextEncoding)
                   ( (dwand (* DrawingWand)) (str :cstring))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetTextUnderColor" DrawSetTextUnderColor)
                   ( (dwand (* DrawingWand)) (wand (* PixelWand)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetViewbox" DrawSetViewbox)
                   ( (dwand (* DrawingWand)) (lint :unsigned-long) (lint2 :unsigned-long) (lint3 :unsigned-long) (lint4 :unsigned-long))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawTranslate" DrawTranslate)
                   ( (dwand (* DrawingWand)) (dint :double) (dint2 :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

;; /*   Deprecated. */
(uffi:def-function ("DrawGetFillOpacity" DrawGetFillOpacity)
                   ( (dwand (* DrawingWand)))
                   :returning :double
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawGetStrokeOpacity" DrawGetStrokeOpacity)
                   ( (dwand (* DrawingWand)))
                   :returning :double
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPeekGraphicWand" DrawPeekGraphicWand)
                   ( (dwand (* DrawingWand)))
                   :returning (* DrawInfo)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPopGraphicContext" DrawPopGraphicContext)
                   ( (dwand (* DrawingWand)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawPushGraphicContext" DrawPushGraphicContext)
                   ( (dwand (* DrawingWand)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetFillOpacity" DrawSetFillOpacity)
                   ( (dwand (* DrawingWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DrawSetStrokeOpacity" DrawSetStrokeOpacity)
                   ( (dwand (* DrawingWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

