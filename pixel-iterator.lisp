(in-package :cl-magick)

;Copyright (c) 2005 Dave Watson

;Permission is hereby granted, free of charge, to any person obtaining
;a copy of this software and associated documentation files (the
;"Software"), to deal in the Software without restriction, including
;without limitation the rights to use, copy, modify, merge, publish,
;distribute, sublicense, and/or sell copies of the Software, and to
;permit persons to whom the Software is furnished to do so, subject to
;the following conditions:

;The above copyright notice and this permission notice shall be
;included in all copies or substantial portions of the Software.

;THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
;LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(uffi:def-function ("PixelGetIteratorException" PixelGetIteratorException)
                   ( (piterator (* PixelIterator)) (exceptiontype (* ExceptionType)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("IsPixelIterator" IsPixelIterator)
                   ( (piterator (* PixelIterator)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelClearIteratorException" PixelClearIteratorException)
                   ( (piterator (* PixelIterator)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetIteratorRow" PixelSetIteratorRow)
                   ( (* PixelIterator) (lint :long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSyncIterator" PixelSyncIterator)
                   ( (piterator (* PixelIterator)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DestroyPixelIterator" DestroyPixelIterator)
                   ( (piterator (* PixelIterator)))
                   :returning (* PixelIterator)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("NewPixelIterator" NewPixelIterator)
                   ( (wand (* MagickWand)))
                   :returning (* PixelIterator)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("NewPixelRegionIterator" NewPixelRegionIterator)
                   ( (wand (* MagickWand)) (lint :long) (lint2 :long) (lint4 :unsigned-long) (lint3 :unsigned-long))
                   :returning (* PixelIterator)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetNextIteratorRow" PixelGetNextIteratorRow)
                   ( (piterator (* PixelIterator)) (lint (* :unsigned-long)))
                   :returning (* PixelWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("ClearPixelIterator" ClearPixelIterator)
                   ( (piterator (* PixelIterator)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelResetIterator" PixelResetIterator)
                   ( (piterator (* PixelIterator)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

;; /*   Deprecated. */
(uffi:def-function ("PixelIteratorGetException" PixelIteratorGetException)
                   ( (piterator (* PixelIterator)) (exceptiontype (* ExceptionType)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetNextRow" PixelGetNextRow)
                   ( (piterator (* PixelIterator)))
                   :returning (* PixelWand)
                   :module "/usr/lib/libWand.so")

