(in-package :CL-MAGICK)

;Copyright (c) 2005 Dave Watson

;Permission is hereby granted, free of charge, to any person obtaining
;a copy of this software and associated documentation files (the
;"Software"), to deal in the Software without restriction, including
;without limitation the rights to use, copy, modify, merge, publish,
;distribute, sublicense, and/or sell copies of the Software, and to
;permit persons to whom the Software is furnished to do so, subject to
;the following conditions:

;The above copyright notice and this permission notice shall be
;included in all copies or substantial portions of the Software.

;THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
;LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(uffi:def-function ("PixelGetException" PixelGetException)
                   ( (wand (* PixelWand)) (exceptionType (* ExceptionType)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetColorAsString" PixelGetColorAsString)
                   ( (* PixelWand))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetAlpha" PixelGetAlpha)
                   ( (wand (* PixelWand)))
                   :returning :double
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetBlack" PixelGetBlack)
                   ( (wand (* PixelWand)))
                   :returning :double
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetBlue" PixelGetBlue)
                   ( (wand (* PixelWand)))
                   :returning :double
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetCyan" PixelGetCyan)
                   ( (wand (* PixelWand)))
                   :returning :double
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetGreen" PixelGetGreen)
                   ( (wand (* PixelWand)))
                   :returning :double
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetMagenta" PixelGetMagenta)
                   ( (wand (* PixelWand)))
                   :returning :double
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetOpacity" PixelGetOpacity)
                   ( (wand (* PixelWand)))
                   :returning :double
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetRed" PixelGetRed)
                   ( (wand (* PixelWand)))
                   :returning :double
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetYellow" PixelGetYellow)
                   ( (wand (* PixelWand)))
                   :returning :double
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetIndex" PixelGetIndex)
                   ( (wand (* PixelWand)))
                   :returning IndexPacket
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("IsPixelWand" IsPixelWand)
                   ( (wand (* PixelWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelClearException" PixelClearException)
                   ( (wand (* PixelWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetColor" PixelSetColor)
                   ( (wand (* PixelWand)) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DestroyPixelWand" DestroyPixelWand)
                   ( (wand (* PixelWand)))
                   :returning (* PixelWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DestroyPixelWands" DestroyPixelWands)
                   ( (* PixelWand  *  wand) (lint :unsigned-long))
                   :returning (* PixelWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("NewPixelWand" NewPixelWand)
                   ()
                   :returning (* PixelWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("NewPixelWands" NewPixelWands)
                   ( (lint :unsigned-long))
                   :returning (* PixelWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetAlphaQuantum" PixelGetAlphaQuantum)
                   ( (wand (* PixelWand)))
                   :returning Quantum
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetBlackQuantum" PixelGetBlackQuantum)
                   ( (wand (* PixelWand)))
                   :returning Quantum
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetBlueQuantum" PixelGetBlueQuantum)
                   ( (wand (* PixelWand)))
                   :returning Quantum
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetCyanQuantum" PixelGetCyanQuantum)
                   ( (wand (* PixelWand)))
                   :returning Quantum
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetGreenQuantum" PixelGetGreenQuantum)
                   ( (wand (* PixelWand)))
                   :returning Quantum
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetMagentaQuantum" PixelGetMagentaQuantum)
                   ( (wand (* PixelWand)))
                   :returning Quantum
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetOpacityQuantum" PixelGetOpacityQuantum)
                   ( (wand (* PixelWand)))
                   :returning Quantum
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetRedQuantum" PixelGetRedQuantum)
                   ( (wand (* PixelWand)))
                   :returning Quantum
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetYellowQuantum" PixelGetYellowQuantum)
                   ( (wand (* PixelWand)))
                   :returning Quantum
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetColorCount" PixelGetColorCount)
                   ( (wand (* PixelWand)))
                   :returning :unsigned-long
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("ClearPixelWand" ClearPixelWand)
                   ( (wand (* PixelWand)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetMagickColor" PixelGetMagickColor)
                   ( (wand (* PixelWand)) (magickpixelpacket (* MagickPixelPacket)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelGetQuantumColor" PixelGetQuantumColor)
                   ( (wand (* PixelWand)) (pixelpacket (* PixelPacket)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetAlpha" PixelSetAlpha)
                   ( (wand (* PixelWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetAlphaQuantum" PixelSetAlphaQuantum)
                   ( (wand (* PixelWand)) (quantum Quantum))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetBlack" PixelSetBlack)
                   ( (wand (* PixelWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetBlackQuantum" PixelSetBlackQuantum)
                   ( (wand (* PixelWand)) (quantum Quantum))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetBlue" PixelSetBlue)
                   ( (wand (* PixelWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetBlueQuantum" PixelSetBlueQuantum)
                   ( (wand (* PixelWand)) (quantum Quantum))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetColorCount" PixelSetColorCount)
                   ( (wand (* PixelWand)) (long :unsigned-int))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetCyan" PixelSetCyan)
                   ( (wand (* PixelWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetCyanQuantum" PixelSetCyanQuantum)
                   ( (wand (* PixelWand)) (quantum Quantum))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetGreen" PixelSetGreen)
                   ( (wand (* PixelWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetGreenQuantum" PixelSetGreenQuantum)
                   ( (wand (* PixelWand)) (quantum Quantum))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetIndex" PixelSetIndex)
                   ( (wand (* PixelWand)) (idxpacket IndexPacket))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetMagenta" PixelSetMagenta)
                   ( (wand (* PixelWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetMagentaQuantum" PixelSetMagentaQuantum)
                   ( (wand (* PixelWand)) (quantum Quantum))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetOpacity" PixelSetOpacity)
                   ( (wand (* PixelWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetOpacityQuantum" PixelSetOpacityQuantum)
                   ( (wand (* PixelWand)) (quantum Quantum))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetQuantumColor" PixelSetQuantumColor)
                   ( (wand (* PixelWand)) (* PixelPacket))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetRed" PixelSetRed)
                   ( (wand (* PixelWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetRedQuantum" PixelSetRedQuantum)
                   ( (wand (* PixelWand)) (quantum Quantum))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetYellow" PixelSetYellow)
                   ( (wand (* PixelWand)) (dint :double))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("PixelSetYellowQuantum" PixelSetYellowQuantum)
                   ( (wand (* PixelWand)) (quantum Quantum))
                   :returning :void
                   :module "/usr/lib/libWand.so")

