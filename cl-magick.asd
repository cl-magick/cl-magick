;;; -*- lisp -*-

(in-package :asdf)

(defsystem cl-magick
  :components ((:file "package")
	       (:file "magick_wand" :depends-on ("package"))
	       (:file "pixel-wand" :depends-on ("magick_wand"))
	       (:file "pixel-iterator" :depends-on ("pixel-wand"))
	       (:file "drawing-wand" :depends-on ("magick_wand"))
	       (:file "cl-magick" :depends-on ("pixel-iterator" "drawing-wand" "pixel-wand" "magick_wand")))
  :depends-on (uffi))
