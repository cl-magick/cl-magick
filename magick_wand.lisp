(in-package :cl-magick)

;Copyright (c) 2005 Dave Watson

;Permission is hereby granted, free of charge, to any person obtaining
;a copy of this software and associated documentation files (the
;"Software"), to deal in the Software without restriction, including
;without limitation the rights to use, copy, modify, merge, publish,
;distribute, sublicense, and/or sell copies of the Software, and to
;permit persons to whom the Software is furnished to do so, subject to
;the following conditions:

;The above copyright notice and this permission notice shall be
;included in all copies or substantial portions of the Software.

;THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
;LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(uffi:def-enum StorageType (:Undefined :Char :Double :Float :Integer :Long :Quantum :Short)
  :separator-string "-")

(uffi:def-enum FilterTypes (:Undefined :Point :Box :Triangle :Hermite :Hanning :Hamming
				       :Blackman :Gaussian :Quadratic :Cubic :Catrom :Mitchell
				       :Lanczos :Bessel :Sinc)
  :separator-string "-")

(uffi:def-function ("MagickGetException" MagickGetException)
                   ( (wand (* MagickWand)) (exception (* ExceptionType)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetFilename" MagickGetFilename)
                   ( (wand (* MagickWand)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetFormat" MagickGetFormat)
                   ( (wand (* MagickWand)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetHomeURL" MagickGetHomeURL)
                   ()
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageAttribute" MagickGetImageAttribute)
                   ( (wand (* MagickWand)) (str :cstring))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageFilename" MagickGetImageFilename)
                   ( (wand (* MagickWand)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageFormat" MagickGetImageFormat)
                   ( (wand (* MagickWand)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageSignature" MagickGetImageSignature)
                   ( (wand (* MagickWand)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickIdentifyImage" MagickIdentifyImage)
                   ( (wand (* MagickWand)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickQueryConfigureOption" MagickQueryConfigureOption)
                   ( (str :cstring))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickQueryConfigureOptions" MagickQueryConfigureOptions)
                   ( (str :cstring) (num (* :unsigned-long)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickQueryFonts" MagickQueryFonts)
                   ( (str :cstring) (num (* :unsigned-long)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickQueryFormats" MagickQueryFormats)
                   ( (str :cstring) (num (* :unsigned-long)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageCompose" MagickGetImageCompose)
                   ( (wand (* MagickWand)))
                   :returning CompositeOperator
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageColorspace" MagickGetImageColorspace)
                   ( (wand (* MagickWand)))
                   :returning ColorspaceType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageCompression" MagickGetImageCompression)
                   ( (wand (* MagickWand)))
                   :returning CompressionType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetCopyright" MagickGetCopyright)
                   ()
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetPackageName" MagickGetPackageName)
                   ()
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetQuantumDepth" MagickGetQuantumDepth)
                   ( (num (* :unsigned-long)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetReleaseDate" MagickGetReleaseDate)
                   ()
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetVersion" MagickGetVersion)
                   ( (num (* :unsigned-long)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageDispose" MagickGetImageDispose)
                   ( (wand (* MagickWand)))
                   :returning DisposeType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageGamma" MagickGetImageGamma)
                   ( (wand (* MagickWand)))
                   :returning :double
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetSamplingFactors" MagickGetSamplingFactors)
                   ( (wand (* MagickWand)) (num (* :unsigned-long)))
                   :returning (* :double)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickQueryFontMetrics" MagickQueryFontMetrics)
                   ( (wand (* MagickWand)) (dwand (* DrawingWand)) (str :cstring))
                   :returning (* :double)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickQueryMultilineFontMetrics" MagickQueryMultilineFontMetrics)
                   ( (wand (* MagickWand)) (dwand (* DrawingWand)) (str :cstring))
                   :returning (* :double)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("GetImageFromMagickWand" GetImageFromMagickWand)
                   ( (wand (* MagickWand)))
                   :returning (* Image)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageType" MagickGetImageType)
                   ( (wand (* MagickWand)))
                   :returning ImageType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetInterlaceScheme" MagickGetInterlaceScheme)
                   ( (wand (* MagickWand)))
                   :returning InterlaceType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageInterlaceScheme" MagickGetImageInterlaceScheme)
                   ( (wand (* MagickWand)))
                   :returning InterlaceType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageIndex" MagickGetImageIndex)
                   ( (wand (* MagickWand)))
                   :returning :long
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("IsMagickWand" IsMagickWand)
                   ( (wand (* MagickWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickAdaptiveThresholdImage" MagickAdaptiveThresholdImage)
                   ( (wand (* MagickWand)) (intlong :unsigned-long) (intlong2 :unsigned-long) (intlong3 :long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickAddImage" MagickAddImage)
                   ( (wand (* MagickWand)) (wand2 (* MagickWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickAddNoiseImage" MagickAddNoiseImage)
                   ( (wand (* MagickWand)) (noisetype NoiseType))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickAffineTransformImage" MagickAffineTransformImage)
                   ( (wand (* MagickWand)) (dwand (* DrawingWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickAnnotateImage" MagickAnnotateImage)
                   ( (wand (* MagickWand)) (dwand (* DrawingWand)) (fdouble :double) (fdouble2 :double) (fdouble3 :double) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickAnimateImages" MagickAnimateImages)
                   ( (wand (* MagickWand)) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickBlackThresholdImage" MagickBlackThresholdImage)
                   ( (wand (* MagickWand)) (pwand (* PixelWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickBlurImage" MagickBlurImage)
                   ( (wand (* MagickWand)) (fdouble :double) (fdouble2 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickBlurImageChannel" MagickBlurImageChannel)
                   ( (wand (* MagickWand)) (chtype ChannelType) (fdouble :double) (fdouble2 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickBorderImage" MagickBorderImage)
                   ( (wand (* MagickWand)) (pwand (* PixelWand)) (intlong :unsigned-long) (intlong2 :unsigned-long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickCharcoalImage" MagickCharcoalImage)
                   ( (wand (* MagickWand)) (fdouble :double) (fdouble2 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickChopImage" MagickChopImage)
                   ( (wand (* MagickWand)) (intlong :unsigned-long) (intlong2 :unsigned-long) (intlong3 :long) (intlong4 :long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickClearException" MagickClearException)
                   ( (wand (* MagickWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickClipImage" MagickClipImage)
                   ( (wand (* MagickWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickClipPathImage" MagickClipPathImage)
                   ( (wand (* MagickWand)) (str :cstring) (bool MagickBooleanType))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickColorFloodfillImage" MagickColorFloodfillImage)
                   ( (wand (* MagickWand)) (pwand (* PixelWand)) (fdouble :double) (pwand2 (* PixelWand)) (intlong :long) (intlong2 :long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickColorizeImage" MagickColorizeImage)
                   ( (wand (* MagickWand)) (pwand (* PixelWand)) (pwand2 (* PixelWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickCommentImage" MagickCommentImage)
                   ( (wand (* MagickWand)) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickCompositeImage" MagickCompositeImage)
                   ( (wand (* MagickWand)) (wand2 (* MagickWand)) (compop CompositeOperator) (intlong :long) (intlong2 :long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickConstituteImage" MagickConstituteImage)
                   ( (wand (* MagickWand)) (intlong :unsigned-long) (intlong2 :unsigned-long) (str :cstring) (stype StorageType) (str2 (* :unsigned-char)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickContrastImage" MagickContrastImage)
                   ( (wand (* MagickWand)) (bool MagickBooleanType))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickConvolveImage" MagickConvolveImage)
                   ( (wand (* MagickWand)) (intlong :unsigned-long) (dnum (* :double)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickConvolveImageChannel" MagickConvolveImageChannel)
                   ( (wand (* MagickWand)) (chtype ChannelType) (intlong :unsigned-long) (dnum (* :double)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickCropImage" MagickCropImage)
                   ( (wand (* MagickWand)) (intlong :unsigned-long) (intlong2 :unsigned-long) (intlong3 :long) (intlong4 :long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickCycleColormapImage" MagickCycleColormapImage)
                   ( (wand (* MagickWand)) (intlong :long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickDespeckleImage" MagickDespeckleImage)
                   ( (wand (* MagickWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickDisplayImage" MagickDisplayImage)
                   ( (wand (* MagickWand)) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickDisplayImages" MagickDisplayImages)
                   ( (wand (* MagickWand)) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickDrawImage" MagickDrawImage)
                   ( (wand (* MagickWand)) (dwand (* DrawingWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickEdgeImage" MagickEdgeImage)
                   ( (wand (* MagickWand)) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickEmbossImage" MagickEmbossImage)
                   ( (wand (* MagickWand)) (fdouble :double) (fdouble2 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickEnhanceImage" MagickEnhanceImage)
                   ( (wand (* MagickWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickEqualizeImage" MagickEqualizeImage)
                   ( (wand (* MagickWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickEvaluateImage" MagickEvaluateImage)
                   ( (wand (* MagickWand)) (evalop MagickEvaluateOperator) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickEvaluateImageChannel" MagickEvaluateImageChannel)
                   ( (wand (* MagickWand)) (chtype ChannelType) (evalop MagickEvaluateOperator) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickFlipImage" MagickFlipImage)
                   ( (wand (* MagickWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickFlopImage" MagickFlopImage)
                   ( (wand (* MagickWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickFrameImage" MagickFrameImage)
                   ( (wand (* MagickWand)) (pwand (* PixelWand)) (intlong :unsigned-long) (intlong2 :unsigned-long) (intlong3 :long) (intlong4 :long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGammaImage" MagickGammaImage)
                   ( (wand (* MagickWand)) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGammaImageChannel" MagickGammaImageChannel)
                   ( (wand (* MagickWand)) (chtype ChannelType) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGaussianBlurImage" MagickGaussianBlurImage)
                   ( (wand (* MagickWand)) (fdouble :double) (fdouble2 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGaussianBlurImageChannel" MagickGaussianBlurImageChannel)
                   ( (wand (* MagickWand)) (chtype ChannelType) (fdouble :double) (fdouble2 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageBackgroundColor" MagickGetImageBackgroundColor)
                   ( (wand (* MagickWand)) (pwand (* PixelWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageBluePrimary" MagickGetImageBluePrimary)
                   ( (wand (* MagickWand)) (dnum (* :double)) (dnum2 (* :double)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageBorderColor" MagickGetImageBorderColor)
                   ( (wand (* MagickWand)) (pwand (* PixelWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageChannelDistortion" MagickGetImageChannelDistortion)
                   ( (wand (* MagickWand)) (wand2 (* MagickWand)) (chtype ChannelType) (metrictype MetricType) (dnum (* :double)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageDistortion" MagickGetImageDistortion)
                   ( (wand (* MagickWand)) (wand2 (* MagickWand)) (metrictype MetricType) (dnum (* :double)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageChannelExtrema" MagickGetImageChannelExtrema)
                   ( (wand (* MagickWand)) (chtype ChannelType) (num (* :unsigned-long)) (num2 (* :unsigned-long)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageChannelMean" MagickGetImageChannelMean)
                   ( (wand (* MagickWand)) (chtype ChannelType) (dnum (* :double)) (dnum2 (* :double)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageColormapColor" MagickGetImageColormapColor)
                   ( (wand (* MagickWand)) (intlong :unsigned-long) (pwand (* PixelWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageExtrema" MagickGetImageExtrema)
                   ( (wand (* MagickWand)) (num (* :unsigned-long)) (num2 (* :unsigned-long)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageGreenPrimary" MagickGetImageGreenPrimary)
                   ( (wand (* MagickWand)) (dnum (* :double)) (dnum2 (* :double)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageMatteColor" MagickGetImageMatteColor)
                   ( (wand (* MagickWand)) (pwand (* PixelWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageMean" MagickGetImageMean)
                   ( (wand (* MagickWand)) (dnum (* :double)) (dnum2 (* :double)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImagePixels" MagickGetImagePixels)
                   ( (wand (* MagickWand)) (intlong :long) (intlong2 :long) (intlong3 :unsigned-long) (intlong4 :unsigned-long) (str :cstring) (stype StorageType) (pixels :pointer-void))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageRedPrimary" MagickGetImageRedPrimary)
                   ( (wand (* MagickWand)) (dnum (* :double)) (dnum2 (* :double)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageResolution" MagickGetImageResolution)
                   ( (wand (* MagickWand)) (dnum (* :double)) (dnum2 (* :double)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageWhitePoint" MagickGetImageWhitePoint)
                   ( (wand (* MagickWand)) (dnum (* :double)) (dnum2 (* :double)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetSize" MagickGetSize)
                   ( (wand (* MagickWand)) (num (* :unsigned-long)) (num2 (* :unsigned-long)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickHasNextImage" MagickHasNextImage)
                   ( (wand (* MagickWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickHasPreviousImage" MagickHasPreviousImage)
                   ( (wand (* MagickWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickImplodeImage" MagickImplodeImage)
                   ( (wand (* MagickWand)) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickLabelImage" MagickLabelImage)
                   ( (wand (* MagickWand)) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickLevelImage" MagickLevelImage)
                   ( (wand (* MagickWand)) (fdouble :double) (fdouble2 :double) (fdouble3 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickLevelImageChannel" MagickLevelImageChannel)
                   ( (wand (* MagickWand)) (chtype ChannelType) (fdouble :double) (fdouble2 :double) (fdouble3 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickMagnifyImage" MagickMagnifyImage)
                   ( (wand (* MagickWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickMapImage" MagickMapImage)
                   ( (wand (* MagickWand)) (wand2 (* MagickWand)) (bool MagickBooleanType))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickMatteFloodfillImage" MagickMatteFloodfillImage)
                   ( (wand (* MagickWand)) (quantum Quantum) (fdouble :double) (pwand (* PixelWand)) (intlong :long) (intlong2 :long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickMedianFilterImage" MagickMedianFilterImage)
                   ( (wand (* MagickWand)) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickMinifyImage" MagickMinifyImage)
                   ( (wand (* MagickWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickModulateImage" MagickModulateImage)
                   ( (wand (* MagickWand)) (fdouble :double) (fdouble2 :double) (fdouble3 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickMotionBlurImage" MagickMotionBlurImage)
                   ( (wand (* MagickWand)) (fdouble :double) (fdouble2 :double) (fdouble3 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickNegateImage" MagickNegateImage)
                   ( (wand (* MagickWand)) (bool MagickBooleanType))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickNegateImageChannel" MagickNegateImageChannel)
                   ( (wand (* MagickWand)) (chtype ChannelType) (bool MagickBooleanType))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickNewImage" MagickNewImage)
                   ( (wand (* MagickWand)) (intlong :unsigned-long) (intlong2 :unsigned-long) (pwand (* PixelWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickNextImage" MagickNextImage)
                   ( (wand (* MagickWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickNormalizeImage" MagickNormalizeImage)
                   ( (wand (* MagickWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickOilPaintImage" MagickOilPaintImage)
                   ( (wand (* MagickWand)) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickPaintOpaqueImage" MagickPaintOpaqueImage)
                   ( (wand (* MagickWand)) (pwand (* PixelWand)) (pwand2 (* PixelWand)) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickPaintTransparentImage" MagickPaintTransparentImage)
                   ( (wand (* MagickWand)) (pwand (* PixelWand)) (quantum Quantum) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickPingImage" MagickPingImage)
                   ( (wand (* MagickWand)) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickPosterizeImage" MagickPosterizeImage)
                   ( (wand (* MagickWand)) (intlong :unsigned-long) (bool MagickBooleanType))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickPreviousImage" MagickPreviousImage)
                   ( (wand (* MagickWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickProfileImage" MagickProfileImage)
                   ( (wand (* MagickWand)) (str :cstring) (str2 (* :unsigned-char)) (intlong :unsigned-long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickQuantizeImage" MagickQuantizeImage)
                   ( (wand (* MagickWand)) (intlong :unsigned-long) (colorspType ColorspaceType) (intlong2 :unsigned-long) (bool MagickBooleanType) (bool2 MagickBooleanType))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickQuantizeImages" MagickQuantizeImages)
                   ( (wand (* MagickWand)) (intlong :unsigned-long) (colorspType ColorspaceType) (intlong2 :unsigned-long) (bool MagickBooleanType) (bool3 MagickBooleanType))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickRadialBlurImage" MagickRadialBlurImage)
                   ( (wand (* MagickWand)) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickRaiseImage" MagickRaiseImage)
                   ( (wand (* MagickWand)) (intlong :unsigned-long) (intlong2 :unsigned-long) (intlong3 :long) (intlong4 :long) (bool MagickBooleanType))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickReadImage" MagickReadImage)
                   ( (wand (* MagickWand)) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickReadImageBlob" MagickReadImageBlob)
                   ( (wand (* MagickWand)) (str (* :unsigned-char)) (length size_t))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickReadImageFile" MagickReadImageFile)
                   ( (wand (* MagickWand)) (file (* FILE)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickReduceNoiseImage" MagickReduceNoiseImage)
                   ( (wand (* MagickWand)) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickRemoveImage" MagickRemoveImage)
                   ( (wand (* MagickWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickResampleImage" MagickResampleImage)
                   ( (wand (* MagickWand)) (fdouble :double) (fdouble2 :double) (filtertypes FilterTypes) (fdouble3 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickResizeImage" MagickResizeImage)
                   ( (wand (* MagickWand)) (intlong :unsigned-long) (intlong2 :unsigned-long) (filtertypes2 FilterTypes) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickRollImage" MagickRollImage)
                   ( (wand (* MagickWand)) (intlong :long) (intlong2 :long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickRotateImage" MagickRotateImage)
                   ( (wand (* MagickWand)) (pwand (* PixelWand)) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSampleImage" MagickSampleImage)
                   ( (wand (* MagickWand)) (intlong :unsigned-long) (intlong2 :unsigned-long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickScaleImage" MagickScaleImage)
                   ( (wand (* MagickWand)) (intlong :unsigned-long) (intlong2 :unsigned-long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSeparateImageChannel" MagickSeparateImageChannel)
                   ( (wand (* MagickWand)) (chtype ChannelType))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSepiaToneImage" MagickSepiaToneImage)
                   ( (wand (* MagickWand)) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetCompressionQuality" MagickSetCompressionQuality)
                   ( (wand (* MagickWand)) (intlong :unsigned-long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetFilename" MagickSetFilename)
                   ( (wand (* MagickWand)) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetFormat" MagickSetFormat)
                   ( (wand (* MagickWand)) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImage" MagickSetImage)
                   ( (wand (* MagickWand)) (wand2 (* MagickWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageBackgroundColor" MagickSetImageBackgroundColor)
                   ( (wand (* MagickWand)) (pwand (* PixelWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageBias" MagickSetImageBias)
                   ( (wand (* MagickWand)) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageBluePrimary" MagickSetImageBluePrimary)
                   ( (wand (* MagickWand)) (fdouble :double) (fdouble2 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageBorderColor" MagickSetImageBorderColor)
                   ( (wand (* MagickWand)) (pwand (* PixelWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageChannelDepth" MagickSetImageChannelDepth)
                   ( (wand (* MagickWand)) (chtype ChannelType) (intlong :unsigned-long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageColormapColor" MagickSetImageColormapColor)
                   ( (wand (* MagickWand)) (intlong :unsigned-long) (pwand (* PixelWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageCompose" MagickSetImageCompose)
                   ( (wand (* MagickWand)) (compop CompositeOperator))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageCompression" MagickSetImageCompression)
                   ( (wand (* MagickWand)) (comptype CompressionType))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageDelay" MagickSetImageDelay)
                   ( (wand (* MagickWand)) (intlong :unsigned-long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageDepth" MagickSetImageDepth)
                   ( (wand (* MagickWand)) (intlong :unsigned-long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageDispose" MagickSetImageDispose)
                   ( (wand (* MagickWand)) (dispType DisposeType))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageColorspace" MagickSetImageColorspace)
                   ( (wand (* MagickWand)) (colorspType ColorspaceType))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageCompressionQuality" MagickSetImageCompressionQuality)
                   ( (wand (* MagickWand)) (intlong :unsigned-long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageGreenPrimary" MagickSetImageGreenPrimary)
                   ( (wand (* MagickWand)) (fdouble :double) (fdouble2 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageGamma" MagickSetImageGamma)
                   ( (wand (* MagickWand)) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageFilename" MagickSetImageFilename)
                   ( (wand (* MagickWand)) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageFormat" MagickSetImageFormat)
                   ( (wand (* MagickWand)) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageIndex" MagickSetImageIndex)
                   ( (wand (* MagickWand)) (intlong :long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageInterlaceScheme" MagickSetImageInterlaceScheme)
                   ( (wand (* MagickWand)) (interlaceType InterlaceType))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageIterations" MagickSetImageIterations)
                   ( (wand (* MagickWand)) (intlong :unsigned-long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageMatteColor" MagickSetImageMatteColor)
                   ( (wand (* MagickWand)) (pwand (* PixelWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageOption" MagickSetImageOption)
                   ( (wand (* MagickWand)) (str :cstring) (str2 :cstring) (str3 :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImagePixels" MagickSetImagePixels)
                   ( (wand (* MagickWand)) (intlong :long) (intlong2 :long) (intlong3 :unsigned-long) (intlong4 :unsigned-long) (str :cstring) (stypeo StorageType) (str2 (* :unsigned-char)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageRedPrimary" MagickSetImageRedPrimary)
                   ( (wand (* MagickWand)) (fdouble :double) (fdouble2 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageRenderingIntent" MagickSetImageRenderingIntent)
                   ( (wand (* MagickWand)) (renderintent RenderingIntent))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageResolution" MagickSetImageResolution)
                   ( (wand (* MagickWand)) (fdouble :double) (fdouble2 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageScene" MagickSetImageScene)
                   ( (wand (* MagickWand)) (intlong :unsigned-long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageType" MagickSetImageType)
                   ( (wand (* MagickWand)) (imgtype ImageType))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageUnits" MagickSetImageUnits)
                   ( (wand (* MagickWand)) (restype ResolutionType))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageVirtualPixelMethod" MagickSetImageVirtualPixelMethod)
                   ( (wand (* MagickWand)) (pixelmethod VirtualPixelMethod))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetPassphrase" MagickSetPassphrase)
                   ( (wand (* MagickWand)) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageProfile" MagickSetImageProfile)
                   ( (wand (* MagickWand)) (str :cstring) (str2 (* :unsigned-char)) (intlong :unsigned-long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetResourceLimit" MagickSetResourceLimit)
                   ( (type ResourceType) (limit :unsigned-long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetSamplingFactors" MagickSetSamplingFactors)
                   ( (wand (* MagickWand)) (intlong :unsigned-long) (dnum (* :double)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetSize" MagickSetSize)
                   ( (wand (* MagickWand)) (intlong :unsigned-long) (intlong2 :unsigned-long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetImageWhitePoint" MagickSetImageWhitePoint)
                   ( (wand (* MagickWand)) (fdouble :double) (fdouble2 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetInterlaceScheme" MagickSetInterlaceScheme)
                   ( (wand (* MagickWand)) (interlaceType InterlaceType))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetResolution" MagickSetResolution)
                   ( (wand (* MagickWand)) (fdouble :double) (fdouble2 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickShadowImage" MagickShadowImage)
                   ( (wand (* MagickWand)) (fdouble :double) (fdouble2 :double) (intlong :long) (intlong2 :long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSharpenImage" MagickSharpenImage)
                   ( (wand (* MagickWand)) (fdouble :double) (fdouble2 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSharpenImageChannel" MagickSharpenImageChannel)
                   ( (wand (* MagickWand)) (chtype ChannelType) (fdouble :double) (fdouble2 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickShaveImage" MagickShaveImage)
                   ( (wand (* MagickWand)) (intlong :unsigned-long) (intlong2 :unsigned-long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickShearImage" MagickShearImage)
                   ( (wand (* MagickWand)) (pwand (* PixelWand)) (fdouble :double) (fdouble2 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSigmoidalContrastImage" MagickSigmoidalContrastImage)
                   ( (wand (* MagickWand)) (bool MagickBooleanType) (fdouble :double) (fdouble2 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSigmoidalContrastImageChannel" MagickSigmoidalContrastImageChannel)
                   ( (wand (* MagickWand)) (chtype ChannelType) (bool MagickBooleanType) (fdouble :double) (fdouble2 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSolarizeImage" MagickSolarizeImage)
                   ( (wand (* MagickWand)) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSpliceImage" MagickSpliceImage)
                   ( (wand (* MagickWand)) (intlong :unsigned-long) (intlong2 :unsigned-long) (intlong3 :long) (intlong4 :long))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSpreadImage" MagickSpreadImage)
                   ( (wand (* MagickWand)) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickStripImage" MagickStripImage)
                   ( (wand (* MagickWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSwirlImage" MagickSwirlImage)
                   ( (wand (* MagickWand)) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickTintImage" MagickTintImage)
                   ( (wand (* MagickWand)) (pwand (* PixelWand)) (pwand2 (* PixelWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickThresholdImage" MagickThresholdImage)
                   ( (wand (* MagickWand)) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickThresholdImageChannel" MagickThresholdImageChannel)
                   ( (wand (* MagickWand)) (chtype ChannelType) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickTrimImage" MagickTrimImage)
                   ( (wand (* MagickWand)) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickUnsharpMaskImage" MagickUnsharpMaskImage)
                   ( (wand (* MagickWand)) (fdouble :double) (fdouble2 :double) (fdouble3 :double) (fdouble4 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickUnsharpMaskImageChannel" MagickUnsharpMaskImageChannel)
                   ( (wand (* MagickWand)) (chtype ChannelType) (fdouble :double) (fdouble2 :double) (fdouble3 :double) (fdouble4 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickWaveImage" MagickWaveImage)
                   ( (wand (* MagickWand)) (fdouble :double) (fdouble2 :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickWhiteThresholdImage" MagickWhiteThresholdImage)
                   ( (wand (* MagickWand)) (pwand (* PixelWand)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickWriteImage" MagickWriteImage)
                   ( (wand (* MagickWand)) (str :cstring))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickWriteImageFile" MagickWriteImageFile)
                   ( (wand (* MagickWand)) (file (* FILE)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickWriteImages" MagickWriteImages)
                   ( (wand (* MagickWand)) (str :cstring) (bool MagickBooleanType))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickWriteImagesFile" MagickWriteImagesFile)
                   ( (wand (* MagickWand)) (file (* FILE)))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetProgressMonitor" MagickSetProgressMonitor)
                   ( (wand (* MagickWand)) (progressmonitor MagickProgressMonitor) (ptr :pointer-void))
                   :returning MagickProgressMonitor
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageSize" MagickGetImageSize)
                   ( (wand (* MagickWand)))
                   :returning MagickSizeType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("CloneMagickWand" CloneMagickWand)
                   ( (wand (* MagickWand)))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("DestroyMagickWand" DestroyMagickWand)
                   ( (wand (* MagickWand)))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickAppendImages" MagickAppendImages)
                   ( (wand (* MagickWand)) (bool MagickBooleanType))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickAverageImages" MagickAverageImages)
                   ( (wand (* MagickWand)))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickCoalesceImages" MagickCoalesceImages)
                   ( (wand (* MagickWand)))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickCombineImages" MagickCombineImages)
                   ( (wand (* MagickWand)) (chtype ChannelType))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickCompareImageChannels" MagickCompareImageChannels)
                   ( (wand (* MagickWand)) (wand2 (* MagickWand)) (chtype ChannelType) (metrictype MetricType) (dnum (* :double)))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickCompareImages" MagickCompareImages)
                   ( (wand (* MagickWand)) (wand2 (* MagickWand)) (metrictype MetricType) (dnum (* :double)))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickDeructImages" MagickDeructImages)
                   ( (wand (* MagickWand)))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickFlattenImages" MagickFlattenImages)
                   ( (wand (* MagickWand)))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickFxImage" MagickFxImage)
                   ( (wand (* MagickWand)) (str :cstring))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickFxImageChannel" MagickFxImageChannel)
                   ( (wand (* MagickWand)) (chtype ChannelType) (str :cstring))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImage" MagickGetImage)
                   ( (wand (* MagickWand)))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickMorphImages" MagickMorphImages)
                   ( (wand (* MagickWand)) (intlong :unsigned-long))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickMosaicImages" MagickMosaicImages)
                   ( (wand (* MagickWand)))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickMontageImage" MagickMontageImage)
                   ( (wand (* MagickWand)) (dwand (* DrawingWand)) (str :cstring) (str2 :cstring) (montagemode MontageMode) (str3 :cstring))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickPreviewImages" MagickPreviewImages)
                   ( (wandwand (* MagickWand)) (previewtype PreviewType))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSteganoImage" MagickSteganoImage)
                   ( (wand (* MagickWand)) (wand2 (* MagickWand)) (intlong :long))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickStereoImage" MagickStereoImage)
                   ( (wand (* MagickWand)) (wand2 (* MagickWand)))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickTextureImage" MagickTextureImage)
                   ( (wand (* MagickWand)) (wand2 (* MagickWand)))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickTransformImage" MagickTransformImage)
                   ( (wand (* MagickWand)) (str :cstring) (str2 :cstring))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("NewMagickWand" NewMagickWand)
                   ()
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("NewMagickWandFromImage" NewMagickWandFromImage)
                   ( (img (* Image)))
                   :returning (* MagickWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageHistogram" MagickGetImageHistogram)
                   ( (wand (* MagickWand)) (num (* :unsigned-long)))
                   :returning (* PixelWand)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageRenderingIntent" MagickGetImageRenderingIntent)
                   ( (wand (* MagickWand)))
                   :returning RenderingIntent
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageUnits" MagickGetImageUnits)
                   ( (wand (* MagickWand)))
                   :returning ResolutionType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageBlob" MagickGetImageBlob)
                   ( (wand (* MagickWand)) (size (* size_t)))
                   :returning (* :unsigned-char)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImagesBlob" MagickGetImagesBlob)
                   ( (wand (* MagickWand)) (size (* size_t)))
                   :returning (* :unsigned-char)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageProfile" MagickGetImageProfile)
                   ( (wand (* MagickWand)) (str :cstring) (num (* :unsigned-long)))
                   :returning (* :unsigned-char)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickRemoveImageProfile" MagickRemoveImageProfile)
                   ( (wand (* MagickWand)) (str :cstring) (num (* :unsigned-long)))
                   :returning (* :unsigned-char)
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageColors" MagickGetImageColors)
                   ( (wand (* MagickWand)))
                   :returning :unsigned-long
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageCompressionQuality" MagickGetImageCompressionQuality)
                   ( (wand (* MagickWand)))
                   :returning :unsigned-long
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageDelay" MagickGetImageDelay)
                   ( (wand (* MagickWand)))
                   :returning :unsigned-long
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageChannelDepth" MagickGetImageChannelDepth)
                   ( (wand (* MagickWand)) (chtype ChannelType))
                   :returning :unsigned-long
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageDepth" MagickGetImageDepth)
                   ( (wand (* MagickWand)))
                   :returning :unsigned-long
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageHeight" MagickGetImageHeight)
                   ( (wand (* MagickWand)))
                   :returning :unsigned-long
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageIterations" MagickGetImageIterations)
                   ( (wand (* MagickWand)))
                   :returning :unsigned-long
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageScene" MagickGetImageScene)
                   ( (wand (* MagickWand)))
                   :returning :unsigned-long
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageWidth" MagickGetImageWidth)
                   ( (wand (* MagickWand)))
                   :returning :unsigned-long
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetNumberImages" MagickGetNumberImages)
                   ( (wand (* MagickWand)))
                   :returning :unsigned-long
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetResourceLimit" MagickGetResourceLimit)
                   ( (resourcetype ResourceType))
                   :returning :unsigned-long
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickGetImageVirtualPixelMethod" MagickGetImageVirtualPixelMethod)
                   ( (wand (* MagickWand)))
                   :returning VirtualPixelMethod
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("ClearMagickWand" ClearMagickWand)
                   ( (wand (* MagickWand)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickRelinquishMemory" MagickRelinquishMemory)
                   ( (ptr :pointer-void))
                   :returning :pointer-void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickResetIterator" MagickResetIterator)
                   ( (wand (* MagickWand)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetFirstIterator" MagickSetFirstIterator)
                   ( (wand (* MagickWand)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickSetLastIterator" MagickSetLastIterator)
                   ( (wand (* MagickWand)))
                   :returning :void
                   :module "/usr/lib/libWand.so")

;; /*   Deprecated methods. */
(uffi:def-function ("MagickDescribeImage" MagickDescribeImage)
                   ( (wand (* MagickWand)))
                   :returning :cstring
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickOpaqueImage" MagickOpaqueImage)
                   ( (wand (* MagickWand)) (pwand (* PixelWand)) (pwand2 (* PixelWand)) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickTransparentImage" MagickTransparentImage)
                   ( (wand (* MagickWand)) (pwand (* PixelWand)) (quantum Quantum) (fdouble :double))
                   :returning MagickBooleanType
                   :module "/usr/lib/libWand.so")

(uffi:def-function ("MagickWriteImageBlob" MagickWriteImageBlob)
                   ( (wand (* MagickWand)) (size (* size_t)))
                   :returning (* :unsigned-char)
                   :module "/usr/lib/libWand.so")

