(in-package :common-lisp-user)

;Copyright (c) 2005 Dave Watson

;Permission is hereby granted, free of charge, to any person obtaining
;a copy of this software and associated documentation files (the
;"Software"), to deal in the Software without restriction, including
;without limitation the rights to use, copy, modify, merge, publish,
;distribute, sublicense, and/or sell copies of the Software, and to
;permit persons to whom the Software is furnished to do so, subject to
;the following conditions:

;The above copyright notice and this permission notice shall be
;included in all copies or substantial portions of the Software.

;THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
;LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(defpackage cl-magick
  (:use :common-lisp)
  (:export 

;;package.lisp
   :CompositeOperator
   :ColorSpaceType
   :CompressionType
   :DisposeType
   :Image
   :ImageType
   :InterlaceType
   :MagickBooleanType
   :ResolutionType
   :NoiseType
   :ChannelType
   :MagickEvaluateOperator
   :MetricType
   :Quantum
   :FilterTypes-Point
   :FilterTypes-Box
   :FilterTypes-Triangle
   :FilterTypes-Hermite
   :FilterTypes-Hanning
   :FilterTypes-Hamming
   :FilterTypes-Blackman
   :FilterTypes-Gaussian
   :FilterTypes-Quadratic
   :FilterTypes-Cubic
   :FilterTypes-Catrom
   :FilterTypes-Mitchell
   :FilterTypes-Lanczos
   :FilterTypes-Bessel
   :FilterTypes-Sinc
   :RenderingIntent
   :VirtualPixelMethod
   :MagickProgressMonitor
   :MagickSizeType
   :MontageMode
   :PreviewType
   :PixelWand
   :ResourceType
   :size_t

;;magick_wand.lisp
   :MagickWand
   :StorageType-Undefined
   :StorageType-Char
   :StorageType-Double
   :StorageType-Float
   :StorageType-Integer
   :StorageType-Long
   :StorageType-Quantum
   :StorageType-Short
   :MagickGetException
   :MagickGetFilename
   :MagickGetFormat
   :MagickGetHomeURL
   :MagickGetImageAttribute
   :MagickGetImageFilename
   :MagickGetImageFormat
   :MagickGetImageSignature
   :MagickIdentifyImage
   :MagickQueryConfigureOption
   :MagickQueryConfigureOptions
   :MagickQueryFonts
   :MagickQueryFormats
   :MagickGetImageCompose
   :MagickGetImageColorspace
   :MagickGetImageCompression
   :MagickGetCopyright
   :MagickGetPackageName
   :MagickGetQuantumDepth
   :MagickGetReleaseDate
   :MagickGetVersion
   :MagickGetImageDispose
   :MagickGetImageGamma
   :MagickGetSamplingFactors
   :MagickQueryFontMetrics
   :MagickQueryMultilineFontMetrics
   :GetImageFromMagickWand
   :MagickGetImageType
   :MagickGetInterlaceScheme
   :MagickGetImageInterlaceScheme
   :MagickGetImageIndex
   :IsMagickWand
   :MagickAdaptiveThresholdImage
   :MagickAddImage
   :MagickAddNoiseImage
   :MagickAffineTransformImage
   :MagickAnnotateImage
   :MagickAnimateImages
   :MagickBlackThresholdImage
   :MagickBlurImage
   :MagickBlurImageChannel
   :MagickBorderImage
   :MagickCharcoalImage
   :MagickChopImage
   :MagickClearException
   :MagickClipImage
   :MagickClipPathImage
   :MagickColorFloodfillImage
   :MagickColorizeImage
   :MagickCommentImage
   :MagickCompositeImage
   :MagickConstituteImage
   :MagickContrastImage
   :MagickConvolveImage
   :MagickConvolveImageChannel
   :MagickCropImage
   :MagickCycleColormapImage
   :MagickDespeckleImage
   :MagickDisplayImage
   :MagickDisplayImages
   :MagickDrawImage
   :MagickEdgeImage
   :MagickEmbossImage
   :MagickEnhanceImage
   :MagickEqualizeImage
   :MagickEvaluateImage
   :MagickEvaluateImageChannel
   :MagickFlipImage
   :MagickFlopImage
   :MagickFrameImage
   :MagickGammaImage
   :MagickGammaImageChannel
   :MagickGaussianBlurImage
   :MagickGaussianBlurImageChannel
   :MagickGetImageBackgroundColor
   :MagickGetImageBluePrimary
   :MagickGetImageBorderColor
   :MagickGetImageChannelDistortion
   :MagickGetImageDistortion
   :MagickGetImageChannelExtrema
   :MagickGetImageChannelMean
   :MagickGetImageColormapColor
   :MagickGetImageExtrema
   :MagickGetImageGreenPrimary
   :MagickGetImageMatteColor
   :MagickGetImageMean
   :MagickGetImagePixels
   :MagickGetImageRedPrimary
   :MagickGetImageResolution
   :MagickGetImageWhitePoint
   :MagickGetSize
   :MagickHasNextImage
   :MagickHasPreviousImage
   :MagickImplodeImage
   :MagickLabelImage
   :MagickLevelImage
   :MagickLevelImageChannel
   :MagickMagnifyImage
   :MagickMapImage
   :MagickMatteFloodfillImage
   :MagickMedianFilterImage
   :MagickMinifyImage
   :MagickModulateImage
   :MagickMotionBlurImage
   :MagickNegateImage
   :MagickNegateImageChannel
   :MagickNewImage
   :MagickNextImage
   :MagickNormalizeImage
   :MagickOilPaintImage
   :MagickPaintOpaqueImage
   :MagickPaintTransparentImage
   :MagickPingImage
   :MagickPosterizeImage
   :MagickPreviousImage
   :MagickProfileImage
   :MagickQuantizeImage
   :MagickQuantizeImages
   :MagickRadialBlurImage
   :MagickRaiseImage
   :MagickReadImage
   :MagickReadImageBlob
   :MagickReadImageFile
   :MagickReduceNoiseImage
   :MagickRemoveImage
   :MagickResampleImage
   :MagickResizeImage
   :MagickRollImage
   :MagickRotateImage
   :MagickSampleImage
   :MagickScaleImage
   :MagickSeparateImageChannel
   :MagickSepiaToneImage
   :MagickSetCompressionQuality
   :MagickSetFilename
   :MagickSetFormat
   :MagickSetImage
   :MagickSetImageBackgroundColor
   :MagickSetImageBias
   :MagickSetImageBluePrimary
   :MagickSetImageBorderColor
   :MagickSetImageChannelDepth
   :MagickSetImageColormapColor
   :MagickSetImageCompose
   :MagickSetImageCompression
   :MagickSetImageDelay
   :MagickSetImageDepth
   :MagickSetImageDispose
   :MagickSetImageColorspace
   :MagickSetImageCompressionQuality
   :MagickSetImageGreenPrimary
   :MagickSetImageGamma
   :MagickSetImageFilename
   :MagickSetImageFormat
   :MagickSetImageIndex
   :MagickSetImageInterlaceScheme
   :MagickSetImageIterations
   :MagickSetImageMatteColor
   :MagickSetImageOption
   :MagickSetImagePixels
   :MagickSetImageRedPrimary
   :MagickSetImageRenderingIntent
   :MagickSetImageResolution
   :MagickSetImageScene
   :MagickSetImageType
   :MagickSetImageUnits
   :MagickSetImageVirtualPixelMethod
   :MagickSetPassphrase
   :MagickSetImageProfile
   :MagickSetResourceLimit
   :MagickSetSamplingFactors
   :MagickSetSize
   :MagickSetImageWhitePoint
   :MagickSetInterlaceScheme
   :MagickSetResolution
   :MagickShadowImage
   :MagickSharpenImage
   :MagickSharpenImageChannel
   :MagickShaveImage
   :MagickShearImage
   :MagickSigmoidalContrastImage
   :MagickSigmoidalContrastImageChannel
   :MagickSolarizeImage
   :MagickSpliceImage
   :MagickSpreadImage
   :MagickStripImage
   :MagickSwirlImage
   :MagickTintImage
   :MagickThresholdImage
   :MagickThresholdImageChannel
   :MagickTrimImage
   :MagickUnsharpMaskImage
   :MagickUnsharpMaskImageChannel
   :MagickWaveImage
   :MagickWhiteThresholdImage
   :MagickWriteImage
   :MagickWriteImageFile
   :MagickWriteImages
   :MagickWriteImagesFile
   :MagickSetProgressMonitor
   :MagickGetImageSize
   :CloneMagickWand
   :DestroyMagickWand
   :MagickAppendImages
   :MagickAverageImages
   :MagickCoalesceImages
   :MagickCombineImages
   :MagickCompareImageChannels
   :MagickCompareImages
   :MagickDeructImages
   :MagickFlattenImages
   :MagickFxImage
   :MagickFxImageChannel
   :MagickGetImage
   :MagickMorphImages
   :MagickMosaicImages
   :MagickMontageImage
   :MagickPreviewImages
   :MagickSteganoImage
   :MagickStereoImage
   :MagickTextureImage
   :MagickTransformImage
   :NewMagickWand
   :NewMagickWandFromImage
   :MagickGetImageHistogram
   :MagickGetImageRenderingIntent
   :MagickGetImageUnits
   :MagickGetImageBlob
   :MagickGetImagesBlob
   :MagickGetImageProfile
   :MagickRemoveImageProfile
   :MagickGetImageColors
   :MagickGetImageCompressionQuality
   :MagickGetImageDelay
   :MagickGetImageChannelDepth
   :MagickGetImageDepth
   :MagickGetImageHeight
   :MagickGetImageIterations
   :MagickGetImageScene
   :MagickGetImageWidth
   :MagickGetNumberImages
   :MagickGetResourceLimit
   :MagickGetImageVirtualPixelMethod
   :ClearMagickWand
   :MagickRelinquishMemory
   :MagickResetIterator
   :MagickSetFirstIterator
   :MagickSetLastIterator
   :MagickDescribeImage
   :MagickOpaqueImage
   :MagickTransparentImage
   :MagickWriteImageBlob

;;pixel-wand.lisp

   :PixelWand
   :ExceptionType
   :IndexPacket
   :PixelGetException
   :PixelGetColorAsString
   :PixelGetAlpha
   :PixelGetBlack
   :PixelGetBlue
   :PixelGetCyan
   :PixelGetGreen
   :PixelGetMagenta
   :PixelGetOpacity
   :PixelGetRed
   :PixelGetYellow
   :PixelGetIndex
   :IsPixelWand
   :PixelClearException
   :PixelSetColor
   :DestroyPixelWand
   :DestroyPixelWands
   :NewPixelWand
   :NewPixelWands
   :PixelGetAlphaQuantum
   :PixelGetBlackQuantum
   :PixelGetBlueQuantum
   :PixelGetCyanQuantum
   :PixelGetGreenQuantum
   :PixelGetMagentaQuantum
   :PixelGetOpacityQuantum
   :PixelGetRedQuantum
   :PixelGetYellowQuantum
   :PixelGetColorCount
   :ClearPixelWand
   :PixelGetMagickColor
   :PixelGetQuantumColor
   :PixelSetAlpha
   :PixelSetAlphaQuantum
   :PixelSetBlack
   :PixelSetBlackQuantum
   :PixelSetBlue
   :PixelSetBlueQuantum
   :PixelSetColorCount
   :PixelSetCyan
   :PixelSetCyanQuantum
   :PixelSetGreen
   :PixelSetGreenQuantum
   :PixelSetIndex
   :PixelSetMagenta
   :PixelSetMagentaQuantum
   :PixelSetOpacity
   :PixelSetOpacityQuantum
   :PixelSetQuantumColor
   :PixelSetRed
   :PixelSetRedQuantum
   :PixelSetYellow
   :PixelSetYellowQuantum

;;   pixel-iterator.lisp
   :PixelIterator
   :PixelGetIteratorException
   :IsPixelIterator
   :PixelClearIteratorException
   :PixelSetIteratorRow
   :PixelSyncIterator
   :DestroyPixelIterator
   :NewPixelIterator
   :NewPixelRegionIterator
   :PixelGetNextIteratorRow
   :ClearPixelIterator
   :PixelResetIterator
   :PixelIteratorGetException
   :PixelGetNextRow

;;drawing-wand.lisp
   :DrawingWand
   :AlignType
   :ClipPathUnits
   :DecorationType
   :DrawInfo
   :FillRule
   :GravityWand
   :GravityType
   :LineCap
   :LineJoin
   :StretchType
   :StyleType
   :DrawGetTextAlignment
   :DrawGetClipPath
   :DrawGetException
   :DrawGetFont
   :DrawGetFontFamily
   :DrawGetTextEncoding
   :DrawGetVectorGraphics
   :DrawGetClipUnits
   :DrawGetTextDecoration
   :DrawGetFillAlpha
   :DrawGetFontSize
   :DrawGetStrokeDashArray
   :DrawGetStrokeDashOffset
   :DrawGetStrokeAlpha
   :DrawGetStrokeWidth
   :PeekDrawingWand
   :CloneDrawingWand
   :DestroyDrawingWand
   :DrawAllocateWand
   :NewDrawingWand
   :DrawGetClipRule
   :DrawGetFillRule
   :DrawGetGravity
   :DrawGetStrokeLineCap
   :DrawGetStrokeLineJoin
   :DrawClearException
   :DrawComposite
   :DrawGetStrokeAntialias
   :DrawGetTextAntialias
   :DrawPopPattern
   :DrawPushPattern
   :DrawRender
   :DrawSetClipPath
   :DrawSetFillPatternURL
   :DrawSetFont
   :DrawSetFontFamily
   :DrawSetStrokeDashArray
   :DrawSetStrokePatternURL
   :DrawSetVectorGraphics
   :IsDrawingWand
   :PopDrawingWand
   :PushDrawingWand
   :DrawGetFontStretch
   :DrawGetFontStyle
   :DrawGetFontWeight
   :DrawGetStrokeMiterLimit
   :ClearDrawingWand
   :DrawAffine
   :DrawAnnotation
   :DrawArc
   :DrawBezier
   :DrawCircle
   :DrawColor
   :DrawComment
   :DrawEllipse
   :DrawGetFillColor
   :DrawGetStrokeColor
   :DrawGetTextUnderColor
   :DrawLine
   :DrawMatte
   :DrawPathClose
   :DrawPathCurveToAbsolute
   :DrawPathCurveToRelative
   :DrawPathCurveToQuadraticBezierAbsolute
   :DrawPathCurveToQuadraticBezierRelative
   :DrawPathCurveToQuadraticBezierSmoothAbsolute
   :DrawPathCurveToQuadraticBezierSmoothRelative
   :DrawPathCurveToSmoothAbsolute
   :DrawPathCurveToSmoothRelative
   :DrawPathEllipticArcAbsolute
   :DrawPathEllipticArcRelative
   :DrawPathFinish
   :DrawPathLineToAbsolute
   :DrawPathLineToRelative
   :DrawPathLineToHorizontalAbsolute
   :DrawPathLineToHorizontalRelative
   :DrawPathLineToVerticalAbsolute
   :DrawPathLineToVerticalRelative
   :DrawPathMoveToAbsolute
   :DrawPathMoveToRelative
   :DrawPathStart
   :DrawPoint
   :DrawPolygon
   :DrawPolyline
   :DrawPopClipPath
   :DrawPopDefs
   :DrawPushClipPath
   :DrawPushDefs
   :DrawRectangle
   :DrawRotate
   :DrawRoundRectangle
   :DrawScale
   :DrawSetClipRule
   :DrawSetClipUnits
   :DrawSetFillColor
   :DrawSetFillAlpha
   :DrawSetFillRule
   :DrawSetFontSize
   :DrawSetFontStretch
   :DrawSetFontStyle
   :DrawSetFontWeight
   :DrawSetGravity
   :DrawSkewX
   :DrawSkewY
   :DrawSetStrokeAntialias
   :DrawSetStrokeColor
   :DrawSetStrokeDashOffset
   :DrawSetStrokeLineCap
   :DrawSetStrokeLineJoin
   :DrawSetStrokeMiterLimit
   :DrawSetStrokeAlpha
   :DrawSetStrokeWidth
   :DrawSetTextAlignment
   :DrawSetTextAntialias
   :DrawSetTextDecoration
   :DrawSetTextEncoding
   :DrawSetTextUnderColor
   :DrawSetViewbox
   :DrawTranslate
   :DrawGetFillOpacity
   :DrawGetStrokeOpacity
   :DrawPeekGraphicWand
   :DrawPopGraphicContext
   :DrawPushGraphicContext
   :DrawSetFillOpacity
   :DrawSetStrokeOpacity

   ;;cl-magick.lisp
   :with-wand
   ))

(in-package :cl-magick)

(uffi:load-foreign-library "/usr/lib/libWand.so" :module "/usr/lib/libWand.so" :supporting-libraries '("c"))

(uffi:def-foreign-type DrawingWand :char)

(uffi:def-foreign-type PixelIterator :char)

(uffi:def-foreign-type PixelWand :char)

(uffi:def-foreign-type ExceptionType :char)

(uffi:def-foreign-type IndexPacket :char)

(uffi:def-foreign-type MagickWand :char)

(uffi:def-foreign-type CompositeOperator :char)

(uffi:def-foreign-type ColorSpaceType :char)

(uffi:def-foreign-type CompressionType :char)

(uffi:def-foreign-type DisposeType :char)

(uffi:def-foreign-type Image :char)

(uffi:def-foreign-type ImageType :char)

(uffi:def-foreign-type InterlaceType :char)

(uffi:def-foreign-type MagickBooleanType :char)

(uffi:def-foreign-type ResolutionType :char)

(uffi:def-foreign-type NoiseType :char)

(uffi:def-foreign-type ChannelType :char)

(uffi:def-foreign-type MagickEvaluateOperator :char)

(uffi:def-foreign-type MetricType :char)

(uffi:def-foreign-type Quantum :char)

(uffi:def-foreign-type RenderingIntent :char)

(uffi:def-foreign-type VirtualPixelMethod :char)

(uffi:def-foreign-type MagickProgressMonitor :char)

(uffi:def-foreign-type MagickSizeType :char)

(uffi:def-foreign-type MontageMode :char)

(uffi:def-foreign-type PreviewType :char)

(uffi:def-foreign-type ResourceType :char)

(uffi:def-foreign-type size_t :int)

(uffi:def-foreign-type AlignType :char)

(uffi:def-foreign-type ClipPathUnits :char)

(uffi:def-foreign-type DecorationType :char)

(uffi:def-foreign-type DrawInfo :char)

(uffi:def-foreign-type FillRule :char)

(uffi:def-foreign-type GravityWand :char)

(uffi:def-foreign-type GravityType :char)

(uffi:def-foreign-type LineCap :char)

(uffi:def-foreign-type LineJoin :char)

(uffi:def-foreign-type StretchType :char)

(uffi:def-foreign-type StyleType :char)

(uffi:def-foreign-type PixelPacket :char)

(uffi:def-foreign-type MagickPixelPacket :char)

(uffi:def-foreign-type File :char)

(uffi:def-foreign-type PointInfo :char)

(uffi:def-foreign-type PaintMethod :char)

(uffi:def-foreign-type AffineMatrix :char)
