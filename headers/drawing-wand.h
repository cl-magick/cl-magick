/*
  Copyright 1999-2005 ImageMagick Studio LLC, a non-profit organization
  dedicated to making software imaging solutions freely available.
  
  You may not use this file except in compliance with the License.
  obtain a copy of the License at
  
    http://www.imagemagick.org/www/Copyright.html
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  ImageMagick drawing wand API.
*/
typedef struct void DrawingWand;
typedef struct void PixelIterator;
typedef struct void PixelWand;
typedef struct void ExceptionType;
typedef struct void IndexPacket;

typedef struct void MagickWand;
typedef struct void CompositeOperator;
typedef struct void ColorSpaceType;
typedef struct void CompressionType;
typedef struct void DisposeType;
typedef struct void Image;
typedef struct void ImageType;
typedef struct void InterlaceType;
typedef struct void MagickBooleanType;
typedef struct void ResolutionType;
typedef struct void NoiseType;
typedef struct void ChannelType;
typedef struct void StorageType;
typedef struct void MagickEvaluateOperator;
typedef struct void MetricType;
typedef struct void Quantum;
typedef struct void FilterTypes;
typedef struct void RenderingIntent;
typedef struct void VirtualPixelMethod;
typedef struct void MagickProgressMonitor;
typedef struct void MagickSizeType;
typedef struct void MontageMode;
typedef struct void PreviewType;
typedef struct void ResourceType;
typedef struct int size_t;

typedef struct void AlignType;
typedef struct void ClipPathUnits;
typedef struct void DecorationType;
typedef struct void DrawInfo;
typedef struct void FillRule;
typedef struct void GravityWand;
typedef struct void GravityType;
typedef struct void LineCap;
typedef struct void LineJoin;
typedef struct void StretchType;
typedef struct void StyleType;

extern WandExport AlignType
  DrawGetTextAlignment(const DrawingWand * dwand);

extern WandExport char
  *DrawGetClipPath(const DrawingWand * dwand),
  *DrawGetException(const DrawingWand * dwand,ExceptionType * exceptiontype),
  *DrawGetFont(const DrawingWand * dwand),
  *DrawGetFontFamily(const DrawingWand * dwand),
  *DrawGetTextEncoding(const DrawingWand * dwand),
  *DrawGetVectorGraphics(const DrawingWand * dwand);

extern WandExport ClipPathUnits
  DrawGetClipUnits(const DrawingWand * dwand);

extern WandExport DecorationType
  DrawGetTextDecoration(const DrawingWand * dwand);

extern WandExport double
  DrawGetFillAlpha(const DrawingWand * dwand),
  DrawGetFontSize(const DrawingWand * dwand),
  *DrawGetStrokeDashArray(const DrawingWand * dwand,unsigned long * lint),
  DrawGetStrokeDashOffset(const DrawingWand * dwand),
  DrawGetStrokeAlpha(const DrawingWand * dwand),
  DrawGetStrokeWidth(const DrawingWand * dwand);

extern WandExport DrawInfo
  *PeekDrawingWand(const DrawingWand * dwand);

extern WandExport DrawingWand
  *CloneDrawingWand(const DrawingWand * dwand),
  *DestroyDrawingWand(DrawingWand * dwand),
  *DrawAllocateWand(const DrawInfo * drawinfo,Image * image),
  *NewDrawingWand(void);

extern WandExport FillRule
  DrawGetClipRule(const DrawingWand * dwand),
  DrawGetFillRule(const DrawingWand * dwand);

extern WandExport GravityType
  DrawGetGravity(const DrawingWand * dwand);

extern WandExport LineCap
  DrawGetStrokeLineCap(const DrawingWand * dwand);

extern WandExport LineJoin
  DrawGetStrokeLineJoin(const DrawingWand * dwand);

extern WandExport MagickBooleanType
  DrawClearException(DrawingWand * dwand),
  DrawComposite(DrawingWand * dwand,const CompositeOperator compositeoperator,const double dint,const double dint2,
    const double dint3,const double dint4,MagickWand * wand),
  DrawGetStrokeAntialias(const DrawingWand * dwand),
  DrawGetTextAntialias(const DrawingWand * dwand),
  DrawPopPattern(DrawingWand * dwand),
  DrawPushPattern(DrawingWand * dwand,const char * str,const double dint,const double dint2,
    const double dint3,const double dint4),
  DrawRender(DrawingWand * dwand),
  DrawSetClipPath(DrawingWand * dwand,const char * str),
  DrawSetFillPatternURL(DrawingWand * dwand,const char * str),
  DrawSetFont(DrawingWand * dwand,const char * str),
  DrawSetFontFamily(DrawingWand * dwand,const char * str),
  DrawSetStrokeDashArray(DrawingWand * dwand,const unsigned long lint,const double * dint),
  DrawSetStrokePatternURL(DrawingWand * dwand,const char * str),
  DrawSetVectorGraphics(DrawingWand * dwand,const char * str),
  IsDrawingWand(const DrawingWand * dwand),
  PopDrawingWand(DrawingWand * dwand),
  PushDrawingWand(DrawingWand * dwand);

extern WandExport StretchType
  DrawGetFontStretch(const DrawingWand * dwand);

extern WandExport StyleType
  DrawGetFontStyle(const DrawingWand * dwand);

extern WandExport unsigned long
  DrawGetFontWeight(const DrawingWand * dwand),
  DrawGetStrokeMiterLimit(const DrawingWand * dwand);

extern WandExport void
  ClearDrawingWand(DrawingWand * dwand),
  DrawAffine(DrawingWand * dwand,const AffineMatrix * affinematrix),
  DrawAnnotation(DrawingWand * dwand,const double dint,const double dint2,const unsigned char * str),
  DrawArc(DrawingWand * dwand,const double dint,const double dint2,const double dint3,const double dint4,
    const double dint5,const double dint6),
  DrawBezier(DrawingWand * dwand,const unsigned long lint,const PointInfo * pinfo),
  DrawCircle(DrawingWand * dwand,const double dint,const double dint2,const double dint3,const double dint4),
  DrawColor(DrawingWand * dwand,const double dint,const double dint2,const PaintMethod paintmethod),
  DrawComment(DrawingWand * dwand,const char *),
  DrawEllipse(DrawingWand * dwand,const double dint,const double dint2,const double dint3,const double dint4,
    const double dint5,const double dint6),
  DrawGetFillColor(const DrawingWand * dwand,PixelWand * wand),
  DrawGetStrokeColor(const DrawingWand * dwand,PixelWand * wand),
  DrawGetTextUnderColor(const DrawingWand * dwand,PixelWand * wand),
  DrawLine(DrawingWand * dwand,const double dint, const double dint2,const double dint3,const double dint4),
  DrawMatte(DrawingWand * dwand,const double dint,const double dint2,const PaintMethod paintmethod),
  DrawPathClose(DrawingWand * dwand),
  DrawPathCurveToAbsolute(DrawingWand * dwand,const double dint,const double dint2,const double dint3,
    const double dint4,const double dint5,const double dint6),
  DrawPathCurveToRelative(DrawingWand * dwand,const double dint,const double dint2,const double dint3,
    const double dint4,const double dint5, const double dint6),
  DrawPathCurveToQuadraticBezierAbsolute(DrawingWand * dwand,const double dint,
    const double dint2,const double dint3,const double dint4),
  DrawPathCurveToQuadraticBezierRelative(DrawingWand * dwand,const double dint,
    const double dint2,const double dint3,const double dint4),
  DrawPathCurveToQuadraticBezierSmoothAbsolute(DrawingWand * dwand,const double dint,
    const double dint2),
  DrawPathCurveToQuadraticBezierSmoothRelative(DrawingWand * dwand,const double dint,
    const double dint2),
  DrawPathCurveToSmoothAbsolute(DrawingWand * dwand,const double dint,const double dint2,
    const double dint3,const double dint4),
  DrawPathCurveToSmoothRelative(DrawingWand * dwand,const double dint,const double dint2,
    const double dint3,const double dint4),
  DrawPathEllipticArcAbsolute(DrawingWand * dwand,const double dint,const double dint2,
    const double dint3,const MagickBooleanType bool,const MagickBooleanType bool2,const double dint4,
    const double dint5),
  DrawPathEllipticArcRelative(DrawingWand * dwand,const double dint,const double dint2,
    const double dint3,const MagickBooleanType bool,const MagickBooleanType bool2,const double dint4,
    const double dint5),
  DrawPathFinish(DrawingWand * dwand),
  DrawPathLineToAbsolute(DrawingWand * dwand,const double dint,const double dint2),
  DrawPathLineToRelative(DrawingWand * dwand,const double dint,const double dint2),
  DrawPathLineToHorizontalAbsolute(DrawingWand * dwand,const double dint),
  DrawPathLineToHorizontalRelative(DrawingWand * dwand,const double dint),
  DrawPathLineToVerticalAbsolute(DrawingWand * dwand,const double dint),
  DrawPathLineToVerticalRelative(DrawingWand * dwand,const double dint),
  DrawPathMoveToAbsolute(DrawingWand * dwand,const double dint,const double dint2),
  DrawPathMoveToRelative(DrawingWand * dwand,const double dint,const double dint2),
  DrawPathStart(DrawingWand * dwand),
  DrawPoint(DrawingWand * dwand,const double dint,const double dint2),
  DrawPolygon(DrawingWand * dwand,const unsigned long lint,const PointInfo * pinfo),
  DrawPolyline(DrawingWand * dwand,const unsigned long lint,const PointInfo * pinfo),
  DrawPopClipPath(DrawingWand * dwand),
  DrawPopDefs(DrawingWand * dwand),
  DrawPushClipPath(DrawingWand * dwand,const char * str),
  DrawPushDefs(DrawingWand * dwand),
  DrawRectangle(DrawingWand * dwand,const double dint,const double dint2,const double dint3,
    const double dint4),
  DrawRotate(DrawingWand * dwand,const double dint),
  DrawRoundRectangle(DrawingWand * dwand,double dint,double dint2,double dint3,double dint4,double dint5,double dint6),
  DrawScale(DrawingWand * dwand,const double dint,const double dint2),
  DrawSetClipRule(DrawingWand * dwand,const FillRule fillrule),
  DrawSetClipUnits(DrawingWand * dwand,const ClipPathUnits pathunits),
  DrawSetFillColor(DrawingWand * dwand,const PixelWand * wand),
  DrawSetFillAlpha(DrawingWand * dwand,const double dint),
  DrawSetFillRule(DrawingWand * dwand,const FillRule fillrule),
  DrawSetFontSize(DrawingWand * dwand,const double dint),
  DrawSetFontStretch(DrawingWand * dwand,const StretchType stretchtype),
  DrawSetFontStyle(DrawingWand * dwand,const StyleType styletype),
  DrawSetFontWeight(DrawingWand * dwand,const unsigned long lint),
  DrawSetGravity(DrawingWand * dwand,const GravityType gravitytype),
  DrawSkewX(DrawingWand * dwand,const double dint),
  DrawSkewY(DrawingWand * dwand,const double dint),
  DrawSetStrokeAntialias(DrawingWand * dwand,const MagickBooleanType bool),
  DrawSetStrokeColor(DrawingWand * dwand,const PixelWand * wand),
  DrawSetStrokeDashOffset(DrawingWand * dwand,const double dashoffset),
  DrawSetStrokeLineCap(DrawingWand * dwand,const LineCap linecap),
  DrawSetStrokeLineJoin(DrawingWand * dwand,const LineJoin linejoin),
  DrawSetStrokeMiterLimit(DrawingWand * dwand,const unsigned long lint),
  DrawSetStrokeAlpha(DrawingWand * dwand, const double dint),
  DrawSetStrokeWidth(DrawingWand * dwand,const double dint),
  DrawSetTextAlignment(DrawingWand * dwand,const AlignType aligntype),
  DrawSetTextAntialias(DrawingWand * dwand,const MagickBooleanType bool),
  DrawSetTextDecoration(DrawingWand * dwand,const DecorationType decorationtype),
  DrawSetTextEncoding(DrawingWand * dwand,const char * str),
  DrawSetTextUnderColor(DrawingWand * dwand,const PixelWand * wand),
  DrawSetViewbox(DrawingWand * dwand,unsigned long lint,unsigned long lint2,unsigned long lint3,
    unsigned long lint4),
  DrawTranslate(DrawingWand * dwand,const double dint,const double dint2);

/*
  Deprecated.
*/
extern WandExport double 
  DrawGetFillOpacity(const DrawingWand * dwand),
  DrawGetStrokeOpacity(const DrawingWand * dwand);

extern WandExport DrawInfo
  *DrawPeekGraphicWand(const DrawingWand * dwand);

extern WandExport void
  DrawPopGraphicContext(DrawingWand * dwand),
  DrawPushGraphicContext(DrawingWand * dwand),
  DrawSetFillOpacity(DrawingWand * dwand,const double dint),
  DrawSetStrokeOpacity(DrawingWand * dwand,const double dint);

