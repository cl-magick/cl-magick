/*
  Copyright 1999-2005 ImageMagick Studio LLC, a non-profit organization
  dedicated to making software imaging solutions freely available.
  
  You may not use this file except in compliance with the License.
  obtain a copy of the License at
  
    http://www.imagemagick.org/www/Copyright.html
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  ImageMagick pixel iterator API.
*/
typedef struct void PixelIterator;
typedef struct void PixelWand;
typedef struct void ExceptionType;
typedef struct void IndexPacket;
typedef struct void MagickWand;
typedef struct void CompositeOperator;
typedef struct void ColorSpaceType;
typedef struct void CompressionType;
typedef struct void DisposeType;
typedef struct void Image;
typedef struct void ImageType;
typedef struct void InterlaceType;
typedef struct void MagickBooleanType;
typedef struct void ResolutionType;
typedef struct void NoiseType;
typedef struct void ChannelType;
typedef struct void StorageType;
typedef struct void MagickEvaluateOperator;
typedef struct void MetricType;
typedef struct void Quantum;
typedef struct void FilterTypes;
typedef struct void RenderingIntent;
typedef struct void VirtualPixelMethod;
typedef struct void MagickProgressMonitor;
typedef struct void MagickSizeType;
typedef struct void MontageMode;
typedef struct void PreviewType;
typedef struct void ResourceType;
typedef struct int size_t;

extern WandExport char
  *PixelGetIteratorException(const PixelIterator * piterator,ExceptionType * exceptiontype);

extern WandExport MagickBooleanType
  IsPixelIterator(const PixelIterator * piterator),
  PixelClearIteratorException(PixelIterator * piterator),
  PixelSetIteratorRow(PixelIterator *,const long lint),
  PixelSyncIterator(PixelIterator * piterator);

extern WandExport PixelIterator
  *DestroyPixelIterator(PixelIterator * piterator),
  *NewPixelIterator(MagickWand * wand),
  *NewPixelRegionIterator(MagickWand * wand,const long lint,const long lint2,
    const unsigned long lint4,const unsigned long lint3);

extern WandExport PixelWand
  **PixelGetNextIteratorRow(PixelIterator * piterator,unsigned long * lint);

extern WandExport void
  ClearPixelIterator(PixelIterator * piterator),
  PixelResetIterator(PixelIterator * piterator);

/*
  Deprecated.
*/
extern WandExport char
  *PixelIteratorGetException(const PixelIterator * piterator,ExceptionType * exceptiontype);

extern WandExport PixelWand
  **PixelGetNextRow(PixelIterator * piterator);

