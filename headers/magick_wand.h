/*
  Copyright 1999-2005 ImageMagick Studio LLC, a non-profit organization
  dedicated to making software imaging solutions freely available.
  
  You may not use this file except in compliance with the License.
  obtain a copy of the License at
  
    http://www.imagemagick.org/www/Copyright.html
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  ImageMagick pixel wand API.
*/

typedef struct void MagickWand;
typedef struct void CompositeOperator;
typedef struct void ColorSpaceType;
typedef struct void CompressionType;
typedef struct void DisposeType;
typedef struct void Image;
typedef struct void ImageType;
typedef struct void InterlaceType;
typedef struct void MagickBooleanType;
typedef struct void ResolutionType;
typedef struct void NoiseType;
typedef struct void ChannelType;
typedef struct void StorageType;
typedef struct void MagickEvaluateOperator;
typedef struct void MetricType;
typedef struct void Quantum;
typedef struct void FilterTypes;
typedef struct void RenderingIntent;
typedef struct void VirtualPixelMethod;
typedef struct void MagickProgressMonitor;
typedef struct void MagickSizeType;
typedef struct void MontageMode;
typedef struct void PreviewType;
typedef struct void PixelWand;
typedef struct void ResourceType;
typedef struct int size_t;

typedef enum
{
  UndefinedPixel,
  CharPixel,
  DoublePixel,
  FloatPixel,
  IntegerPixel,
  LongPixel,
  QuantumPixel,
  ShortPixel
} StorageType;


extern WandExport char
  *MagickGetException(MagickWand * wand,ExceptionType * exception),
  *MagickGetFilename(const MagickWand * wand),
  *MagickGetFormat(MagickWand * wand),
  *MagickGetHomeURL(void),
  *MagickGetImageAttribute(MagickWand * wand,const char * str),
  *MagickGetImageFilename(MagickWand * wand),
  *MagickGetImageFormat(MagickWand * wand),
  *MagickGetImageSignature(MagickWand * wand),
  *MagickIdentifyImage(MagickWand * wand),
  *MagickQueryConfigureOption(const char * str),
  **MagickQueryConfigureOptions(const char * str,unsigned long * num),
  **MagickQueryFonts(const char * str,unsigned long * num),
  **MagickQueryFormats(const char * str,unsigned long * num);

extern WandExport CompositeOperator
  MagickGetImageCompose(MagickWand * wand);

extern WandExport ColorspaceType
  MagickGetImageColorspace(MagickWand * wand);

extern WandExport CompressionType
  MagickGetImageCompression(MagickWand * wand);

extern WandExport const char
  *MagickGetCopyright(void),
  *MagickGetPackageName(void),
  *MagickGetQuantumDepth(unsigned long * num),
  *MagickGetReleaseDate(void),
  *MagickGetVersion(unsigned long * num);

extern WandExport DisposeType
  MagickGetImageDispose(MagickWand * wand);

extern WandExport double
  MagickGetImageGamma(MagickWand * wand),
  *MagickGetSamplingFactors(MagickWand * wand,unsigned long * num),
  *MagickQueryFontMetrics(MagickWand * wand,const DrawingWand * dwand,const char * str),
  *MagickQueryMultilineFontMetrics(MagickWand * wand,const DrawingWand * dwand,
    const char * str);

extern WandExport Image
  *GetImageFromMagickWand(MagickWand * wand);

extern WandExport ImageType
  MagickGetImageType(MagickWand * wand);

extern WandExport InterlaceType
  MagickGetInterlaceScheme(MagickWand * wand),
  MagickGetImageInterlaceScheme(MagickWand * wand);

extern WandExport long
  MagickGetImageIndex(MagickWand * wand);

extern WandExport MagickBooleanType
  IsMagickWand(const MagickWand * wand),
  MagickAdaptiveThresholdImage(MagickWand * wand,const unsigned long intlong,
    const unsigned long intlong2,const long intlong3),
  MagickAddImage(MagickWand * wand,const MagickWand * wand2),
  MagickAddNoiseImage(MagickWand * wand,const NoiseType noisetype),
  MagickAffineTransformImage(MagickWand * wand,const DrawingWand *  dwand),
  MagickAnnotateImage(MagickWand * wand,const DrawingWand * dwand,const double fdouble,
    const double fdouble2,const double fdouble3,const char * str),
  MagickAnimateImages(MagickWand * wand,const char * str),
  MagickBlackThresholdImage(MagickWand * wand,const PixelWand * pwand),
  MagickBlurImage(MagickWand * wand,const double fdouble,const double fdouble2),
  MagickBlurImageChannel(MagickWand * wand,const ChannelType chtype,const double fdouble,
    const double fdouble2),
  MagickBorderImage(MagickWand * wand,const PixelWand * pwand,const unsigned long intlong,
    const unsigned long intlong2),
  MagickCharcoalImage(MagickWand * wand,const double fdouble,const double fdouble2),
  MagickChopImage(MagickWand * wand,const unsigned long intlong,const unsigned long intlong2,
    const long intlong3,const long intlong4),
  MagickClearException(MagickWand * wand),
  MagickClipImage(MagickWand * wand),
  MagickClipPathImage(MagickWand * wand,const char * str,const MagickBooleanType bool),
  MagickColorFloodfillImage(MagickWand * wand,const PixelWand * pwand,const double fdouble,
    const PixelWand * pwand2,const long intlong,const long intlong2),
  MagickColorizeImage(MagickWand * wand,const PixelWand * pwand,const PixelWand * pwand2),
  MagickCommentImage(MagickWand * wand,const char * str),
  MagickCompositeImage(MagickWand * wand,const MagickWand * wand2,const CompositeOperator compop,
    const long intlong,const long intlong2),
  MagickConstituteImage(MagickWand * wand,const unsigned long intlong,const unsigned long intlong2,
    const char * str,const StorageType stype,unsigned char * str2),
  MagickContrastImage(MagickWand * wand,const MagickBooleanType bool),
  MagickConvolveImage(MagickWand * wand,const unsigned long intlong,const double * dnum),
  MagickConvolveImageChannel(MagickWand * wand,const ChannelType chtype,const unsigned long intlong,
    const double * dnum),
  MagickCropImage(MagickWand * wand,const unsigned long intlong,const unsigned long intlong2,
    const long intlong3,const long intlong4),
  MagickCycleColormapImage(MagickWand * wand,const long intlong),
  MagickDespeckleImage(MagickWand * wand),
  MagickDisplayImage(MagickWand * wand,const char * str),
  MagickDisplayImages(MagickWand * wand,const char * str),
  MagickDrawImage(MagickWand * wand,const DrawingWand *  dwand),
  MagickEdgeImage(MagickWand * wand,const double fdouble),
  MagickEmbossImage(MagickWand * wand,const double fdouble,const double fdouble2),
  MagickEnhanceImage(MagickWand * wand),
  MagickEqualizeImage(MagickWand * wand),
  MagickEvaluateImage(MagickWand * wand,const MagickEvaluateOperator evalop,const double fdouble),
  MagickEvaluateImageChannel(MagickWand * wand,const ChannelType chtype,
    const MagickEvaluateOperator evalop,const double fdouble),
  MagickFlipImage(MagickWand * wand),
  MagickFlopImage(MagickWand * wand),
  MagickFrameImage(MagickWand * wand,const PixelWand * pwand,const unsigned long intlong,
    const unsigned long intlong2,const long intlong3,const long intlong4),
  MagickGammaImage(MagickWand * wand,const double fdouble),
  MagickGammaImageChannel(MagickWand * wand,const ChannelType chtype,const double fdouble),
  MagickGaussianBlurImage(MagickWand * wand,const double fdouble,const double fdouble2),
  MagickGaussianBlurImageChannel(MagickWand * wand,const ChannelType chtype,const double fdouble,
    const double fdouble2),
  MagickGetImageBackgroundColor(MagickWand * wand,PixelWand * pwand),
  MagickGetImageBluePrimary(MagickWand * wand,double * dnum,double * dnum2),
  MagickGetImageBorderColor(MagickWand * wand,PixelWand * pwand),
  MagickGetImageChannelDistortion(MagickWand * wand,const MagickWand * wand2,
    const ChannelType chtype, const MetricType metrictype,double * dnum),
  MagickGetImageDistortion(MagickWand * wand,const MagickWand * wand2,const MetricType metrictype,
    double * dnum),
  MagickGetImageChannelExtrema(MagickWand * wand,const ChannelType chtype,unsigned long * num,
    unsigned long * num2),
  MagickGetImageChannelMean(MagickWand * wand,const ChannelType chtype,double * dnum,double * dnum2),
  MagickGetImageColormapColor(MagickWand * wand,const unsigned long intlong,PixelWand * pwand),
  MagickGetImageExtrema(MagickWand * wand,unsigned long * num,unsigned long * num2),
  MagickGetImageGreenPrimary(MagickWand * wand,double * dnum,double * dnum2),
  MagickGetImageMatteColor(MagickWand * wand,PixelWand * pwand),
  MagickGetImageMean(MagickWand * wand,double * dnum,double * dnum2),
  MagickGetImagePixels(MagickWand * wand,const long intlong,const long intlong2,const unsigned long intlong3,
    const unsigned long intlong4,const char * str,const StorageType stype,unsigned char * str2),
  MagickGetImageRedPrimary(MagickWand * wand,double * dnum,double * dnum2),
  MagickGetImageResolution(MagickWand * wand,double * dnum,double * dnum2),
  MagickGetImageWhitePoint(MagickWand * wand,double * dnum,double * dnum2),
  MagickGetSize(const MagickWand * wand,unsigned long * num,unsigned long * num2),
  MagickHasNextImage(MagickWand * wand),
  MagickHasPreviousImage(MagickWand * wand),
  MagickImplodeImage(MagickWand * wand,const double fdouble),
  MagickLabelImage(MagickWand * wand,const char * str),
  MagickLevelImage(MagickWand * wand,const double fdouble,const double fdouble2,const double fdouble3),
  MagickLevelImageChannel(MagickWand * wand,const ChannelType chtype,const double fdouble,
    const double fdouble2,const double fdouble3),
  MagickMagnifyImage(MagickWand * wand),
  MagickMapImage(MagickWand * wand,const MagickWand * wand2,const MagickBooleanType bool),
  MagickMatteFloodfillImage(MagickWand * wand,const Quantum quantum,const double fdouble,
    const PixelWand * pwand,const long intlong,const long intlong2),
  MagickMedianFilterImage(MagickWand * wand,const double fdouble),
  MagickMinifyImage(MagickWand * wand),
  MagickModulateImage(MagickWand * wand,const double fdouble,const double fdouble2,const double fdouble3),
  MagickMotionBlurImage(MagickWand * wand,const double fdouble,const double fdouble2,const double fdouble3),
  MagickNegateImage(MagickWand * wand,const MagickBooleanType bool),
  MagickNegateImageChannel(MagickWand * wand,const ChannelType chtype,
    const MagickBooleanType bool),
  MagickNewImage(MagickWand * wand,const unsigned long intlong,const unsigned long intlong2,
    const PixelWand * pwand),
  MagickNextImage(MagickWand * wand),
  MagickNormalizeImage(MagickWand * wand),
  MagickOilPaintImage(MagickWand * wand,const double fdouble),
  MagickPaintOpaqueImage(MagickWand * wand,const PixelWand * pwand,const PixelWand * pwand2,
    const double fdouble),
  MagickPaintTransparentImage(MagickWand * wand,const PixelWand * pwand,const Quantum quantum,
    const double fdouble),
  MagickPingImage(MagickWand * wand,const char * str),
  MagickPosterizeImage(MagickWand * wand,const unsigned long intlong,
    const MagickBooleanType bool),
  MagickPreviousImage(MagickWand * wand),
  MagickProfileImage(MagickWand * wand,const char * str,const unsigned char * str2,
    const unsigned long intlong),
  MagickQuantizeImage(MagickWand * wand,const unsigned long intlong,const ColorspaceType colorspType,
    const unsigned long intlong2,const MagickBooleanType bool,const MagickBooleanType bool2),
  MagickQuantizeImages(MagickWand * wand,const unsigned long intlong,const ColorspaceType colorspType,
    const unsigned long intlong2,const MagickBooleanType bool,const MagickBooleanType bool3),
  MagickRadialBlurImage(MagickWand * wand,const double fdouble),
  MagickRaiseImage(MagickWand * wand,const unsigned long intlong,const unsigned long intlong2,
    const long intlong3,const long intlong4,const MagickBooleanType bool),
  MagickReadImage(MagickWand * wand,const char * str),
  MagickReadImageBlob(MagickWand * wand,const unsigned char * str,const size_t length),
  MagickReadImageFile(MagickWand * wand,FILE * file),
  MagickReduceNoiseImage(MagickWand * wand,const double fdouble),
  MagickRemoveImage(MagickWand * wand),
  MagickResampleImage(MagickWand * wand,const double fdouble,const double fdouble2,const FilterTypes filtertypes,
    const double fdouble3),
  MagickResizeImage(MagickWand * wand,const unsigned long intlong,const unsigned long intlong2,
    const FilterTypes filtertypes2,const double fdouble),
  MagickRollImage(MagickWand * wand,const long intlong,const long intlong2),
  MagickRotateImage(MagickWand * wand,const PixelWand * pwand,const double fdouble),
  MagickSampleImage(MagickWand * wand,const unsigned long intlong,const unsigned long intlong2),
  MagickScaleImage(MagickWand * wand,const unsigned long intlong,const unsigned long intlong2),
  MagickSeparateImageChannel(MagickWand * wand,const ChannelType chtype),
  MagickSepiaToneImage(MagickWand * wand,const double fdouble),
  MagickSetCompressionQuality(MagickWand * wand,const unsigned long intlong),
  MagickSetFilename(MagickWand * wand,const char * str),
  MagickSetFormat(MagickWand * wand,const char * str),
  MagickSetImage(MagickWand * wand,const MagickWand * wand2),
  MagickSetImageBackgroundColor(MagickWand * wand,const PixelWand * pwand),
  MagickSetImageBias(MagickWand * wand,const double fdouble),
  MagickSetImageBluePrimary(MagickWand * wand,const double fdouble,const double fdouble2),
  MagickSetImageBorderColor(MagickWand * wand,const PixelWand * pwand),
  MagickSetImageChannelDepth(MagickWand * wand,const ChannelType chtype,
    const unsigned long intlong),
  MagickSetImageColormapColor(MagickWand * wand,const unsigned long intlong,
    const PixelWand * pwand),
  MagickSetImageCompose(MagickWand * wand,const CompositeOperator compop),
  MagickSetImageCompression(MagickWand * wand,const CompressionType comptype),
  MagickSetImageDelay(MagickWand * wand,const unsigned long intlong),
  MagickSetImageDepth(MagickWand * wand,const unsigned long intlong),
  MagickSetImageDispose(MagickWand * wand,const DisposeType dispType),
  MagickSetImageColorspace(MagickWand * wand,const ColorspaceType colorspType),
  MagickSetImageCompressionQuality(MagickWand * wand,const unsigned long intlong),
  MagickSetImageGreenPrimary(MagickWand * wand,const double fdouble,const double fdouble2),
  MagickSetImageGamma(MagickWand * wand,const double fdouble),
  MagickSetImageFilename(MagickWand * wand,const char * str),
  MagickSetImageFormat(MagickWand * wand,const char * str),
  MagickSetImageIndex(MagickWand * wand,const long intlong),
  MagickSetImageInterlaceScheme(MagickWand * wand,const InterlaceType interlaceType),
  MagickSetImageIterations(MagickWand * wand,const unsigned long intlong),
  MagickSetImageMatteColor(MagickWand * wand,const PixelWand * pwand),
  MagickSetImageOption(MagickWand * wand,const char * str,const char * str2,const char * str3),
  MagickSetImagePixels(MagickWand * wand,const long intlong,const long intlong2,const unsigned long intlong3,
    const unsigned long intlong4,const char * str,const StorageType stypeo,unsigned char * str2),
  MagickSetImageRedPrimary(MagickWand * wand,const double fdouble,const double fdouble2),
  MagickSetImageRenderingIntent(MagickWand * wand,const RenderingIntent renderintent),
  MagickSetImageResolution(MagickWand * wand,const double fdouble,const double fdouble2),
  MagickSetImageScene(MagickWand * wand,const unsigned long intlong),
  MagickSetImageType(MagickWand * wand,const ImageType imgtype),
  MagickSetImageUnits(MagickWand * wand,const ResolutionType restype),
  MagickSetImageVirtualPixelMethod(MagickWand * wand,const VirtualPixelMethod pixelmethod),
  MagickSetPassphrase(MagickWand * wand,const char * str),
  MagickSetImageProfile(MagickWand * wand,const char * str,const unsigned char * str2,
    const unsigned long intlong),
  MagickSetResourceLimit(const ResourceType type,const unsigned long limit),
  MagickSetSamplingFactors(MagickWand * wand,const unsigned long intlong,const double * dnum),
  MagickSetSize(MagickWand * wand,const unsigned long intlong,const unsigned long intlong2),
  MagickSetImageWhitePoint(MagickWand * wand,const double fdouble,const double fdouble2),
  MagickSetInterlaceScheme(MagickWand * wand,const InterlaceType interlaceType),
  MagickSetResolution(MagickWand * wand,const double fdouble,const double fdouble2),
  MagickShadowImage(MagickWand * wand,const double fdouble,const double fdouble2,const long intlong,
    const long intlong2),
  MagickSharpenImage(MagickWand * wand,const double fdouble,const double fdouble2),
  MagickSharpenImageChannel(MagickWand * wand,const ChannelType chtype,const double fdouble,
    const double fdouble2),
  MagickShaveImage(MagickWand * wand,const unsigned long intlong,const unsigned long intlong2),
  MagickShearImage(MagickWand * wand,const PixelWand * pwand,const double fdouble,const double fdouble2),
  MagickSigmoidalContrastImage(MagickWand * wand,const MagickBooleanType bool,
    const double fdouble,const double fdouble2),
  MagickSigmoidalContrastImageChannel(MagickWand * wand,const ChannelType chtype,
    const MagickBooleanType bool,const double fdouble,const double fdouble2),
  MagickSolarizeImage(MagickWand * wand,const double fdouble),
  MagickSpliceImage(MagickWand * wand,const unsigned long intlong,const unsigned long intlong2,
    const long intlong3,const long intlong4),
  MagickSpreadImage(MagickWand * wand,const double fdouble),
  MagickStripImage(MagickWand * wand),
  MagickSwirlImage(MagickWand * wand,const double fdouble),
  MagickTintImage(MagickWand * wand,const PixelWand * pwand,const PixelWand * pwand2),
  MagickThresholdImage(MagickWand * wand,const double fdouble),
  MagickThresholdImageChannel(MagickWand * wand,const ChannelType chtype,const double fdouble),
  MagickTrimImage(MagickWand * wand,const double fdouble),
  MagickUnsharpMaskImage(MagickWand * wand,const double fdouble,const double fdouble2,const double fdouble3,
    const double fdouble4),
  MagickUnsharpMaskImageChannel(MagickWand * wand,const ChannelType chtype,const double fdouble,
    const double fdouble2,const double fdouble3,const double fdouble4),
  MagickWaveImage(MagickWand * wand,const double fdouble,const double fdouble2),
  MagickWhiteThresholdImage(MagickWand * wand,const PixelWand * pwand),
  MagickWriteImage(MagickWand * wand,const char * str),
  MagickWriteImageFile(MagickWand * wand,FILE * file),
  MagickWriteImages(MagickWand * wand,const char * str,const MagickBooleanType bool),
  MagickWriteImagesFile(MagickWand * wand,FILE * file);

WandExport MagickProgressMonitor
  MagickSetProgressMonitor(MagickWand * wand,const MagickProgressMonitor progressmonitor,void * ptr);

extern WandExport MagickSizeType
  MagickGetImageSize(MagickWand * wand);

extern WandExport MagickWand
  *CloneMagickWand(const MagickWand * wand),
  *DestroyMagickWand(MagickWand * wand),
  *MagickAppendImages(MagickWand * wand,const MagickBooleanType bool),
  *MagickAverageImages(MagickWand * wand),
  *MagickCoalesceImages(MagickWand * wand),
  *MagickCombineImages(MagickWand * wand,const ChannelType chtype),
  *MagickCompareImageChannels(MagickWand * wand,const MagickWand * wand2,const ChannelType chtype,
    const MetricType metrictype,double * dnum),
  *MagickCompareImages(MagickWand * wand,const MagickWand * wand2,const MetricType metrictype,
    double * dnum),
  *MagickDeconstructImages(MagickWand * wand),
  *MagickFlattenImages(MagickWand * wand),
  *MagickFxImage(MagickWand * wand,const char * str),
  *MagickFxImageChannel(MagickWand * wand,const ChannelType chtype,const char * str),
  *MagickGetImage(MagickWand * wand),
  *MagickMorphImages(MagickWand * wand,const unsigned long intlong),
  *MagickMosaicImages(MagickWand * wand),
  *MagickMontageImage(MagickWand * wand,const DrawingWand * dwand,const char * str,
    const char * str2,const MontageMode montagemode,const char * str3),
  *MagickPreviewImages(MagickWand * wandwand,const PreviewType previewtype),
  *MagickSteganoImage(MagickWand * wand,const MagickWand * wand2,const long intlong),
  *MagickStereoImage(MagickWand * wand,const MagickWand * wand2),
  *MagickTextureImage(MagickWand * wand,const MagickWand * wand2),
  *MagickTransformImage(MagickWand * wand,const char * str,const char * str2),
  *NewMagickWand(void),
  *NewMagickWandFromImage(const Image * img);

extern WandExport PixelWand
  **MagickGetImageHistogram(MagickWand * wand,unsigned long * num);

extern WandExport RenderingIntent
  MagickGetImageRenderingIntent(MagickWand * wand);

extern WandExport ResolutionType
  MagickGetImageUnits(MagickWand * wand);

extern WandExport unsigned char
  *MagickGetImageBlob(MagickWand * wand,size_t * size),
  *MagickGetImagesBlob(MagickWand * wand,size_t * size),
  *MagickGetImageProfile(MagickWand * wand,const char * str,unsigned long * num),
  *MagickRemoveImageProfile(MagickWand * wand,const char * str,unsigned long * num);

extern WandExport unsigned long
  MagickGetImageColors(MagickWand * wand),
  MagickGetImageCompressionQuality(MagickWand * wand),
  MagickGetImageDelay(MagickWand * wand),
  MagickGetImageChannelDepth(MagickWand * wand,const ChannelType chtype),
  MagickGetImageDepth(MagickWand * wand),
  MagickGetImageHeight(MagickWand * wand),
  MagickGetImageIterations(MagickWand * wand),
  MagickGetImageScene(MagickWand * wand),
  MagickGetImageWidth(MagickWand * wand),
  MagickGetNumberImages(MagickWand * wand),
  MagickGetResourceLimit(const ResourceType resourcetype);

extern WandExport VirtualPixelMethod
  MagickGetImageVirtualPixelMethod(MagickWand * wand);

extern WandExport void
  ClearMagickWand(MagickWand * wand),
  *MagickRelinquishMemory(void * ptr),
  MagickResetIterator(MagickWand * wand),
  MagickSetFirstIterator(MagickWand * wand),
  MagickSetLastIterator(MagickWand * wand);

/*
  Deprecated methods.
*/
extern WandExport char
  *MagickDescribeImage(MagickWand * wand);

extern WandExport MagickBooleanType
  MagickOpaqueImage(MagickWand * wand,const PixelWand * pwand,const PixelWand * pwand2,
    const double fdouble),
  MagickTransparentImage(MagickWand * wand,const PixelWand * pwand,const Quantum quantum,
    const double fdouble);

extern WandExport unsigned char
  *MagickWriteImageBlob(MagickWand * wand,size_t * size);

