/*
  Copyright 1999-2005 ImageMagick Studio LLC, a non-profit organization
  dedicated to making software imaging solutions freely available.
  
  You may not use this file except in compliance with the License.
  obtain a copy of the License at
  
    http://www.imagemagick.org/www/Copyright.html
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  ImageMagick pixel wand API.
*/


typedef struct void PixelWand;
typedef struct void ExceptionType;
typedef struct void IndexPacket;
typedef struct void MagickWand;
typedef struct void CompositeOperator;
typedef struct void ColorSpaceType;
typedef struct void CompressionType;
typedef struct void DisposeType;
typedef struct void Image;
typedef struct void ImageType;
typedef struct void InterlaceType;
typedef struct void MagickBooleanType;
typedef struct void ResolutionType;
typedef struct void NoiseType;
typedef struct void ChannelType;
typedef struct void StorageType;
typedef struct void MagickEvaluateOperator;
typedef struct void MetricType;
typedef struct void Quantum;
typedef struct void FilterTypes;
typedef struct void RenderingIntent;
typedef struct void VirtualPixelMethod;
typedef struct void MagickProgressMonitor;
typedef struct void MagickSizeType;
typedef struct void MontageMode;
typedef struct void PreviewType;
typedef struct void ResourceType;
typedef struct int size_t;


extern WandExport char
  *PixelGetException(const PixelWand * wand,ExceptionType * exceptionType),
  *PixelGetColorAsString(const PixelWand *);

extern WandExport double
  PixelGetAlpha(const PixelWand * wand),
  PixelGetBlack(const PixelWand * wand),
  PixelGetBlue(const PixelWand * wand),
  PixelGetCyan(const PixelWand * wand),
  PixelGetGreen(const PixelWand * wand),
  PixelGetMagenta(const PixelWand * wand),
  PixelGetOpacity(const PixelWand * wand),
  PixelGetRed(const PixelWand * wand),
  PixelGetYellow(const PixelWand * wand);

extern WandExport IndexPacket
  PixelGetIndex(const PixelWand * wand);

extern WandExport MagickBooleanType
  IsPixelWand(const PixelWand * wand),
  PixelClearException(PixelWand * wand),
  PixelSetColor(PixelWand * wand,const char * str);

extern WandExport PixelWand
  *DestroyPixelWand(PixelWand * wand),
  **DestroyPixelWands(PixelWand * wand*,const unsigned long lint),
  *NewPixelWand(void),
  **NewPixelWands(const unsigned long lint);

extern WandExport Quantum
  PixelGetAlphaQuantum(const PixelWand * wand),
  PixelGetBlackQuantum(const PixelWand * wand),
  PixelGetBlueQuantum(const PixelWand * wand),
  PixelGetCyanQuantum(const PixelWand * wand),
  PixelGetGreenQuantum(const PixelWand * wand),
  PixelGetMagentaQuantum(const PixelWand * wand),
  PixelGetOpacityQuantum(const PixelWand * wand),
  PixelGetRedQuantum(const PixelWand * wand),
  PixelGetYellowQuantum(const PixelWand * wand);

extern WandExport unsigned long
  PixelGetColorCount(const PixelWand * wand);

extern WandExport void
  ClearPixelWand(PixelWand * wand),
  PixelGetMagickColor(const PixelWand * wand,MagickPixelPacket * magickpixelpacket),
  PixelGetQuantumColor(const PixelWand * wand,PixelPacket * pixelpacket),
  PixelSetAlpha(PixelWand * wand,const double dint),
  PixelSetAlphaQuantum(PixelWand * wand,const Quantum quantum),
  PixelSetBlack(PixelWand * wand,const double dint),
  PixelSetBlackQuantum(PixelWand * wand,const Quantum quantum),
  PixelSetBlue(PixelWand * wand,const double dint),
  PixelSetBlueQuantum(PixelWand * wand,const Quantum quantum),
  PixelSetColorCount(PixelWand * wand,const unsigned long),
  PixelSetCyan(PixelWand * wand,const double dint),
  PixelSetCyanQuantum(PixelWand * wand,const Quantum quantum),
  PixelSetGreen(PixelWand * wand,const double dint),
  PixelSetGreenQuantum(PixelWand * wand,const Quantum quantum),
  PixelSetIndex(PixelWand * wand,const IndexPacket idxpacket),
  PixelSetMagenta(PixelWand * wand,const double dint),
  PixelSetMagentaQuantum(PixelWand * wand,const Quantum quantum),
  PixelSetOpacity(PixelWand * wand,const double dint),
  PixelSetOpacityQuantum(PixelWand * wand,const Quantum quantum),
  PixelSetQuantumColor(PixelWand * wand,const PixelPacket *),
  PixelSetRed(PixelWand * wand,const double dint),
  PixelSetRedQuantum(PixelWand * wand,const Quantum quantum),
  PixelSetYellow(PixelWand * wand,const double dint),
  PixelSetYellowQuantum(PixelWand * wand,const Quantum quantum);

